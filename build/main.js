webpackJsonp([0],{

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_session__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_player__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__helper_fireStoreHelper__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_storageservice__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_user__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__game_game__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__models_move__ = __webpack_require__(423);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, sanitizer, db, navParams, storageService, alertCtrl, events, toastCtrl) {
        this.navCtrl = navCtrl;
        this.sanitizer = sanitizer;
        this.db = db;
        this.navParams = navParams;
        this.storageService = storageService;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.toastCtrl = toastCtrl;
        this.sessionScreen = this.navParams.get("sessionScreen");
    }
    HomePage.prototype.getSessionUrl = function () {
        return "https://martor.kobzr.com?sid=" + this.sessionId;
    };
    HomePage.prototype.copyToClipboard = function () {
        var el = document.createElement('textarea');
        el.value = this.getSessionUrl();
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };
    HomePage.prototype.setPlayers = function () {
        if (this.session) {
            this.session.players = [];
            var player1 = new __WEBPACK_IMPORTED_MODULE_5__models_player__["a" /* Player */]();
            player1.index = 0;
            player1.name = "Player 1";
            this.session.players.push(player1);
            var player2 = new __WEBPACK_IMPORTED_MODULE_5__models_player__["a" /* Player */]();
            player2.index = 1;
            player2.name = "Player 2";
            this.session.players.push(player2);
            var player3 = new __WEBPACK_IMPORTED_MODULE_5__models_player__["a" /* Player */]();
            player3.index = 2;
            player3.name = "Player 3";
            this.session.players.push(player3);
            var player4 = new __WEBPACK_IMPORTED_MODULE_5__models_player__["a" /* Player */]();
            player4.index = 3;
            player4.name = "Player 4";
            this.session.players.push(player4);
        }
    };
    HomePage.prototype.newSession = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'New Session',
            inputs: [
                {
                    name: 'name',
                    placeholder: 'Name',
                    value: ''
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Create',
                    handler: function (data) { _this.createSession(data); }
                }
            ]
        });
        alert.present();
    };
    HomePage.prototype.joinSession = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Enter session name',
            inputs: [
                {
                    name: 'name',
                    placeholder: 'Name',
                    value: ''
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Join',
                    handler: function (data) { _this.joinTheSession(data); }
                }
            ]
        });
        alert.present();
    };
    HomePage.prototype.selectLevel = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Level',
            message: 'Select Level',
            inputs: [
                {
                    type: 'radio',
                    label: 'Beginner',
                    value: '40'
                },
                {
                    type: 'radio',
                    label: 'Intermediate',
                    value: '60'
                },
                {
                    type: 'radio',
                    label: 'Advance',
                    value: '80'
                }
            ],
            buttons: [
                {
                    text: "Set",
                    handler: function (data) {
                        _this.session.level = parseInt(data);
                    }
                }
            ]
        });
        prompt.present();
    };
    HomePage.prototype.getLevel = function () {
        if (this.session.level == 40) {
            return "Beginner";
        }
        else if (this.session.level == 60) {
            return "Intermediate";
        }
        else {
            return "Advance";
        }
    };
    HomePage.prototype.joinTheSession = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.setSession(data.name);
                return [2 /*return*/];
            });
        });
    };
    HomePage.prototype.setSession = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var sessionSet, _a, player1, toastOption;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.storageService.setSession(name)];
                    case 1:
                        sessionSet = _b.sent();
                        if (!sessionSet) return [3 /*break*/, 3];
                        _a = this;
                        return [4 /*yield*/, this.storageService.getSession()];
                    case 2:
                        _a.session = _b.sent();
                        this.sessionId = name;
                        this.setPlayers();
                        this.sessionScreen = true;
                        if (this.sessionScreen) {
                            player1 = new __WEBPACK_IMPORTED_MODULE_5__models_player__["a" /* Player */]();
                            this.playerClicked(player1);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        toastOption = {
                            message: "Unable to join sesson",
                            duration: 3000
                        };
                        this.toastCtrl.create(toastOption).present();
                        _b.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.createSession = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var regex, exists, s, created, toastOption, toastOption, ex_1, toastOption;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 9, , 10]);
                        regex = new RegExp(/^[a-zA-Z0-9]+$/);
                        if (!regex.test(data.name)) return [3 /*break*/, 7];
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_6__helper_fireStoreHelper__["a" /* FireStoreHelper */].checkIfDocumentExists(this.db, "session", data.name)];
                    case 1:
                        exists = _a.sent();
                        if (!!exists) return [3 /*break*/, 5];
                        s = new __WEBPACK_IMPORTED_MODULE_4__models_session__["a" /* Session */]();
                        s.id = data.name;
                        s.createdBy = this.user.id;
                        s.startGame = false;
                        s.waitingPlayers = [];
                        s.parentSession = data.name;
                        s.activeSession = data.name;
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_6__helper_fireStoreHelper__["a" /* FireStoreHelper */].set(this.db, "session", s, data.name)];
                    case 2:
                        created = _a.sent();
                        if (!created) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.setSession(data.name)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        toastOption = {
                            message: "Session name already exists",
                            duration: 2000
                        };
                        this.toastCtrl.create(toastOption).present();
                        _a.label = 6;
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        toastOption = {
                            message: "Only letters and numbers allowed in session name",
                            duration: 5000
                        };
                        this.toastCtrl.create(toastOption).present();
                        _a.label = 8;
                    case 8: return [3 /*break*/, 10];
                    case 9:
                        ex_1 = _a.sent();
                        toastOption = {
                            message: "Something went wrong please try again later",
                            duration: 2000
                        };
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    // isHost(): boolean {
    //   return this.user.id === this.session.createdBy;
    // }
    HomePage.prototype.canStartGame = function () {
        if (this.session.players) {
            return this.session.players.filter(function (x) { return x.isBooked === true; }).length >= 2;
        }
        else {
            return false;
        }
    };
    //player data
    HomePage.prototype.playerClicked = function (player) {
        var _this = this;
        var slotTaken = false;
        for (var _i = 0, _a = this.session.players; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.id == this.user.id) {
                console.log(item);
                slotTaken = true;
                break;
            }
        }
        if (!slotTaken) {
            if (this.user.name) {
                player.isBooked = true;
                player.name = this.user.name;
                player.id = this.user.id;
                player.command = "joined";
                this.addPlayer(player);
            }
            else {
                var alert_1 = this.alertCtrl.create({
                    title: 'Enter Name',
                    inputs: [
                        {
                            name: 'name',
                            placeholder: 'Name',
                            value: this.user.name
                        }
                    ],
                    buttons: [
                        {
                            text: 'Cancel',
                            role: 'cancel',
                            handler: function (data) {
                                console.log('Cancel clicked');
                            }
                        },
                        {
                            text: 'Ok',
                            handler: function (data) {
                                player.isBooked = true;
                                player.name = data.name;
                                player.id = _this.user.id;
                                player.command = "joined";
                                _this.addPlayer(player); //yaha se doc me bhijwana he moves k
                                //if fb is implemented change in this setting is required
                                if (data != _this.user.name) {
                                    _this.user.name = data.name;
                                    _this.storageService.setUser(_this.user);
                                    _this.db.collection("users").doc(_this.user.id).set({ name: _this.user.name, id: _this.user.id }, { merge: true });
                                }
                            }
                        }
                    ]
                });
                alert_1.present();
            }
        }
    };
    HomePage.prototype.reset = function () {
        this.setPlayers();
        this.updateSession();
    };
    HomePage.prototype.updateSession = function () {
        __WEBPACK_IMPORTED_MODULE_6__helper_fireStoreHelper__["a" /* FireStoreHelper */].set(this.db, "session", this.session, this.sessionId);
    };
    HomePage.prototype.addPlayer = function (player) {
        return __awaiter(this, void 0, void 0, function () {
            var move, id;
            var _this = this;
            return __generator(this, function (_a) {
                {
                    move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                    move.data = player;
                    move.command = "joined";
                    id = "playerJoined";
                    __WEBPACK_IMPORTED_MODULE_6__helper_fireStoreHelper__["a" /* FireStoreHelper */].subCol(this.db, this.sessionId, move, "" + new Date().getTime() + id)
                        .then(function () {
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__game_game__["a" /* GamePage */], { sessionId: _this.sessionId, session: _this.session, startGame: false });
                    });
                }
                return [2 /*return*/];
            });
        });
    };
    HomePage.prototype.getGameId = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.db.collection("users").doc(id).snapshotChanges()
                    .subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
                    var playerDoc, user;
                    return __generator(this, function (_a) {
                        playerDoc = data.payload.data();
                        user = Object.assign(new __WEBPACK_IMPORTED_MODULE_8__models_user__["a" /* User */](), playerDoc);
                        if (user.gameId !== "") {
                            window.location.href = window.location.origin + "?sid=" + user.gameId;
                        }
                        return [2 /*return*/];
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    HomePage.prototype.playWithBot = function () {
        var session = this.storageService.getBotSession();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__game_game__["a" /* GamePage */], { sessionId: session.id, session: session, startGame: true, bot: true });
    };
    HomePage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var search, obj, _a, _b, sid, _c, player1;
            var _this = this;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        search = window.location.search;
                        if (!window.location.search) return [3 /*break*/, 2];
                        search = search.substring(1, search.length);
                        obj = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
                        if (!(obj.sid && !this.navParams.get("isQuit"))) return [3 /*break*/, 2];
                        console.log(obj.sid);
                        return [4 /*yield*/, this.storageService.setSession(obj.sid)];
                    case 1:
                        _d.sent();
                        _d.label = 2;
                    case 2:
                        if (!!this.navParams.get("isQuit")) return [3 /*break*/, 6];
                        _a = this;
                        return [4 /*yield*/, this.storageService.getUser(false)];
                    case 3:
                        _a.user = _d.sent();
                        _b = this;
                        return [4 /*yield*/, this.storageService.getSession()];
                    case 4:
                        _b.session = _d.sent();
                        sid = window.location.search;
                        if (!(sid === "")) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.getGameId(this.user.id)];
                    case 5:
                        _d.sent();
                        _d.label = 6;
                    case 6:
                        if (!(this.storageService.sessionId && !this.navParams.get("isQuit"))) return [3 /*break*/, 8];
                        _c = this;
                        return [4 /*yield*/, this.storageService.sessionId];
                    case 7:
                        _c.sessionId = _d.sent();
                        this.setPlayers();
                        this.sessionScreen = true;
                        if (this.sessionScreen) {
                            player1 = new __WEBPACK_IMPORTED_MODULE_5__models_player__["a" /* Player */]();
                            this.playerClicked(player1);
                        }
                        _d.label = 8;
                    case 8:
                        this.events.subscribe("new-game", function (data) {
                            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__game_game__["a" /* GamePage */], { gameId: data, sessionId: _this.sessionId, session: _this.session });
                        });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\one\Desktop\projects\martor\src\pages\home\home.html"*/'<ion-content>\n\n\n\n  <ion-item>\n\n    <button ion-button (click)="newSession()" color="secondary">New Session</button>\n\n    <button ion-button (click)="joinSession()" color="secondary">Join Session</button>\n\n    <button ion-button color="secondary" (click)="playWithBot()">Play with Bot</button>\n\n  </ion-item>\n\n  <ion-item *ngIf="sessionId">\n\n    <h1>{{sessionId}}</h1>\n\n    <h4>{{getSessionUrl()}}</h4>\n\n    <button (click)="copyToClipboard()">Copy</button>\n\n  </ion-item>\n\n  <!-- <ion-item *ngIf="sessionId">\n\n    \n\n    <button *ngFor="let player of session.players" [disabled]="player.isBooked" ion-button (click)="playerClicked(player)" color="primary">{{player.name}}</button>\n\n  </ion-item> -->\n\n  <ion-item *ngIf="sessionId">\n\n   \n\n    <button ion-button color="secondary" (click)="reset()">Reset</button>\n\n    <button ion-button color="secondary"  [disabled]="!canStartGame()" (click)="start()">Start Game</button>\n\n    <button ion-button color="secondary" (click)="selectLevel()">{{getLevel()}}</button>\n\n  </ion-item>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\one\Desktop\projects\martor\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["AngularFirestore"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_7__services_storageservice__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Session; });
var Session = /** @class */ (function () {
    function Session() {
        this.level = 40;
        this.sessionCount = 0;
    }
    return Session;
}());

//# sourceMappingURL=session.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Player; });
var Player = /** @class */ (function () {
    function Player() {
        this.cards = [];
        this.playerCards = [];
        this.isBooked = false;
        this.bidCount = -1;
        this.seriesUsed = [];
        this.timeElapse = 0;
        this.isBot = false;
        this.myMove = false;
    }
    Player.arrageById = function (id, list) {
        var players = [];
        var found = false;
        var temp = [];
        for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
            var item = list_1[_i];
            var player = Object.assign(new Player(), item);
            if (player.id == id) {
                players.push(player);
                found = true;
            }
            else if (found) {
                players.push(player);
            }
            else {
                temp.push(player);
            }
        }
        players = players.concat(temp);
        return players;
    };
    return Player;
}());

//# sourceMappingURL=player.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FireStoreHelper; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_firebase__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__node_modules_firebase__);
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};

var FireStoreHelper = /** @class */ (function () {
    function FireStoreHelper() {
    }
    FireStoreHelper.get = function (db, collection, id) {
        return db.collection(collection).doc(id).ref.get();
    };
    FireStoreHelper.getMovesCol = function (db, sessionId) {
        return db.collection("session").doc(sessionId).ref.get();
    };
    FireStoreHelper.checkIfDocumentExists = function (db, collection, id) {
        return new Promise(function (resolve) {
            db.collection(collection).doc(id).ref.get().then(function (doc) {
                resolve(doc.exists);
            }).catch(function (error) {
                error(error);
            });
        });
    };
    FireStoreHelper.checkIfSubDocumentExists = function (db, collection, id) {
        return new Promise(function (resolve) {
            //db.collection<T>(`session`).doc(collection).collection<T>(`moves`).doc(id).set(object)
            db.collection("session").doc(collection).collection("moves").doc(id).ref.get().then(function (doc) {
                resolve(doc.exists);
            }).catch(function (error) {
                error(error);
            });
        });
    };
    FireStoreHelper.update = function (db, collectionName, entity, id) {
        var object = FireStoreHelper.convertToObject(entity);
        object.lastUpdateTime = __WEBPACK_IMPORTED_MODULE_0__node_modules_firebase___default.a.firestore.FieldValue.serverTimestamp();
        db.collection(collectionName).doc(id).update(object).then(function (obj) {
        }).catch(function (error) {
            console.log(error);
        });
    };
    FireStoreHelper.setWithCallback = function (db, collectionName, entity, id) {
        var object = FireStoreHelper.convertToObject(entity);
        object.lastUpdateTime = __WEBPACK_IMPORTED_MODULE_0__node_modules_firebase___default.a.firestore.FieldValue.serverTimestamp();
        return db.collection(collectionName).doc(id).set(object, { merge: true });
    };
    FireStoreHelper.convertToObject = function (entity) {
        var object = {};
        var keys = Object.keys(entity);
        if (keys[0] == "0") {
            object = entity;
        }
        else {
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key = keys_1[_i];
                if (entity[key]) {
                    if (entity[key].constructor === Array) {
                        object[key] = [];
                        for (var _a = 0, _b = entity[key]; _a < _b.length; _a++) {
                            var item = _b[_a];
                            object[key].push(FireStoreHelper.convertToObject(item));
                        }
                    }
                    else if (typeof entity[key] === 'object') {
                        object[key] = FireStoreHelper.convertToObject(entity[key]);
                    }
                    else {
                        object[key] = entity[key];
                    }
                }
                else {
                    object[key] = entity[key];
                }
            }
        }
        return object;
    };
    FireStoreHelper.add = function (db, collectionName, entity) {
        var object = FireStoreHelper.convertToObject(entity);
        return new Promise(function (resolve) {
            object.lastUpdateTime = __WEBPACK_IMPORTED_MODULE_0__node_modules_firebase___default.a.firestore.FieldValue.serverTimestamp();
            db.collection(collectionName).add(object).then(function (doc) {
                resolve(true);
            }).catch(function (error) {
                resolve(false);
            });
        });
    };
    FireStoreHelper.getSubCollectionDocument = function (db, collectionName, documentId, subCollectionName, subDocumentId) {
        return new Promise(function (resolve) {
            db.collection(collectionName).doc(documentId).collection(subCollectionName).doc(subDocumentId).ref.get().then(function (doc) {
                resolve(doc);
            }).catch(function (error) {
                error(error);
            });
        });
    };
    FireStoreHelper.getDocument = function (db, collectionName, documentId) {
        return new Promise(function (resolve) {
            db.collection(collectionName).doc(documentId).ref.get().then(function (doc) {
                resolve(doc);
            }).catch(function (error) {
                error(error);
            });
        });
    };
    FireStoreHelper.set = function (db, collectionName, entity, id) {
        var object = FireStoreHelper.convertToObject(entity);
        object.lastUpdateTime = __WEBPACK_IMPORTED_MODULE_0__node_modules_firebase___default.a.firestore.FieldValue.serverTimestamp();
        return new Promise(function (resolve) {
            db.collection(collectionName).doc(id).set(object).then(function (data) {
                FireStoreHelper.checkIfDocumentExists(db, collectionName, id).then(function (exists) {
                    resolve(exists);
                }).catch(function (error) {
                    error(error);
                });
            }).catch(function (error) {
                error(error);
            });
        });
    };
    FireStoreHelper.setUser = function (db, collectionName, entity, id) {
        var object = FireStoreHelper.convertToObject(entity);
        object.lastUpdateTime = __WEBPACK_IMPORTED_MODULE_0__node_modules_firebase___default.a.firestore.FieldValue.serverTimestamp();
        return new Promise(function (resolve) {
            db.collection(collectionName).doc(id).set(object).catch(function (error) {
                error(error);
            });
        });
    };
    FireStoreHelper.subCol = function (db, collectionName, entity, id) {
        var object = FireStoreHelper.convertToObject(entity);
        object.lastUpdateTime = __WEBPACK_IMPORTED_MODULE_0__node_modules_firebase___default.a.firestore.FieldValue.serverTimestamp();
        return new Promise(function (resolve) {
            db.collection("session").doc(collectionName).collection("moves").doc(id).set(object, { merge: true })
                .then(function (data) {
                console.log(data);
                FireStoreHelper.checkIfSubDocumentExists(db, collectionName, id).then(function (exists) {
                    resolve(exists);
                }).catch(function (error) {
                    error(error);
                });
            }).catch(function (error) {
                error(error);
            });
        });
    };
    FireStoreHelper.deleteAll = function (db, collectionName) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        return db.collection(collectionName).get().subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
                            var _i, _a, doc;
                            return __generator(this, function (_b) {
                                for (_i = 0, _a = data.docs; _i < _a.length; _i++) {
                                    doc = _a[_i];
                                    FireStoreHelper.delete(db, collectionName, doc.id);
                                }
                                setTimeout(function () {
                                    resolve(true);
                                }, 5000);
                                return [2 /*return*/];
                            });
                        }); });
                    })];
            });
        });
    };
    FireStoreHelper.delete = function (db, collectionName, id) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        db.collection(collectionName).doc(id).delete().then(function () {
                            resolve(true);
                        }).catch(function (error) {
                            resolve(false);
                        });
                    })];
            });
        });
    };
    FireStoreHelper.subscribeDocument = function (db, collectionName, id) {
        return db.collection(collectionName).doc(id).valueChanges();
    };
    FireStoreHelper.subscribeCollectionSnapshot = function (db, collectionName) {
        return db.collection("session").doc(collectionName).collection("moves").snapshotChanges();
    };
    FireStoreHelper.subscribeCollection = function (db, collectionName) {
        return db.collection(collectionName).snapshotChanges();
    };
    return FireStoreHelper;
}());

//# sourceMappingURL=fireStoreHelper.js.map

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 203:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 203;

/***/ }),

/***/ 245:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 245;

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GamePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_player__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_deck__ = __webpack_require__(627);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_card__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_storageservice__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__models_session__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__models_move__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__models_user__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__models_turn__ = __webpack_require__(628);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__models_hand__ = __webpack_require__(629);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__home_home__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__models_message__ = __webpack_require__(630);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_random_name__ = __webpack_require__(631);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_random_name___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_17_random_name__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


















// import { firestore } from "firebase";
// import { getPackedSettings } from "http2";
// import { type } from "os";
var GamePage = /** @class */ (function () {
    function GamePage(navCtrl, sanitizer, db, navParams, storageService, alertCtrl, actionCtrl, events, toastCtrl) {
        this.navCtrl = navCtrl;
        this.sanitizer = sanitizer;
        this.db = db;
        this.navParams = navParams;
        this.storageService = storageService;
        this.alertCtrl = alertCtrl;
        this.actionCtrl = actionCtrl;
        this.events = events;
        this.toastCtrl = toastCtrl;
        this.messages = [];
        this.player = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
        this.startGame = false;
        this.players = [];
        this.waitingPlayers = [];
        this.arrangeCards = false;
        this.biddingDone = false;
        this.isBiddingComplete = false;
        this.marCount = 0;
        this.mar = 0;
        this.firstTurn = true;
        this.turnCards = [];
        this.command = "";
        this.moveCount = 0;
        this.turnIndex = -1;
        this.hands = [];
        this.moveIds = [];
        this.gameWon = false;
        this.gameOver = false;
        this.cardDistance = 25;
        this.partOfGame = true;
        this.bot = false;
        this.isHeight = false;
        this.messageText = "";
        this.chatBoxWidth = "60%";
        this.chatBoxHeight = "60%";
        this.cardsTop = "82%";
        this.lastResult = "";
        this.timeCount = 0;
        this.alreadyStarted = true;
        this.globalPlayers = [];
        this.showBidButton = true;
        this.sessionId = this.navParams.get("sessionId");
        this.startGame = this.navParams.get("startGame");
        this.session = this.navParams.get("session");
        this.bot = this.navParams.get("bot");
        this.user = this.storageService.getUser(false);
    }
    GamePage_1 = GamePage;
    GamePage.prototype.showactionsheet = function () {
        var _this = this;
        var actionsheet = null;
        actionsheet = this.actionCtrl.create({
            title: "Options",
            buttons: [
                {
                    text: "Quit",
                    role: "quit",
                    handler: function () {
                        _this.reset();
                    }
                },
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: function () { }
                }
            ]
        });
        actionsheet.present();
    };
    GamePage.prototype.onFileUpload = function (link) {
        var message = new __WEBPACK_IMPORTED_MODULE_16__models_message__["a" /* Message */]();
        message.type = 1;
        message.text = link;
        this.pushMessage(message);
    };
    GamePage.prototype.reset = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.bot) return [3 /*break*/, 1];
                        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__home_home__["a" /* HomePage */], {});
                        return [3 /*break*/, 3];
                    case 1:
                        if (this.gameSubscription) {
                            this.gameSubscription.unsubscribe();
                        }
                        return [4 /*yield*/, this.playerQuit()];
                    case 2:
                        _a.sent();
                        this.storageService.resetSession();
                        window.location.href = window.location.origin;
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.getBidResult = function (player) {
        if ((!this.biddingDone && player.bidCount >= 0) ||
            (this.bidWinner && this.bidWinner.id == player.id)) {
            return true;
        }
        else {
            return false;
        }
    };
    GamePage.prototype.setPlayers = function () {
        if (this.session) {
            this.players = [];
            var player1 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
            player1.index = 0;
            player1.name = "Player 1";
            this.players.push(player1);
            var player2 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
            player2.index = 1;
            player2.name = "Player 2";
            this.players.push(player2);
            var player3 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
            player3.index = 2;
            player3.name = "Player 3";
            this.players.push(player3);
            var player4 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
            player4.index = 3;
            player4.name = "Player 4";
            this.players.push(player4);
        }
    };
    GamePage.prototype.canSeeCards = function (card) {
        if (this.partOfGame) {
            return true;
        }
        else {
            if (this.turnCards.filter(function (x) { return x.card.name == card.name; }).length > 0) {
                return true;
            }
            else {
                return false;
            }
        }
    };
    GamePage.prototype.isBidWinner = function () {
        return this.bidWinner && this.bidWinner.id == this.user.id;
    };
    GamePage.prototype.distribute = function (move) {
        var _this = this;
        if (!this.players) {
            this.players = [];
        }
        else {
            this.players.length = 0;
        }
        for (var _i = 0, _a = move.data.players; _i < _a.length; _i++) {
            var item = _a[_i];
            var player = Object.assign(new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */](), item);
            this.players.push(player);
            this.globalPlayers.push(player);
        }
        if (this.players.filter(function (x) { return x.id == _this.user.id; }).length == 0) {
            this.partOfGame = false;
        }
        this.players = __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */].arrageById(this.user.id, this.players);
        if (this.players.length == 3) {
            this.cardDistance = 19;
        }
        var count = 0;
        for (var _b = 0, _c = this.players; _b < _c.length; _b++) {
            var player = _c[_b];
            if (count == 0) {
                __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */].arrangeCards(player, this.board.offsetWidth, true, 0, 100);
            }
            else if (count == 1) {
                __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */].arrangeCards(player, this.board.offsetWidth, false, 90, 0);
            }
            else if (count == 2) {
                __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */].arrangeCards(player, this.board.offsetWidth, false, 0, 0);
            }
            else if (count == 3) {
                __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */].arrangeCards(player, this.board.offsetWidth, false, 90, 0);
            }
            count++;
        }
        this.arrangeCards = true;
    };
    GamePage.prototype.checkForTheHost = function () {
        return __awaiter(this, void 0, void 0, function () {
            var isHostExists, move, id, document_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        isHostExists = false;
                        if (!this.session.waitingPlayers) return [3 /*break*/, 3];
                        this.session.waitingPlayers.map(function (value, index) {
                            if (value.isHost) {
                                isHostExists = true;
                            }
                        });
                        if (!!isHostExists) return [3 /*break*/, 3];
                        if (!(this.session.waitingPlayers.length > 0 && this.session.waitingPlayers[0].id === this.user.id)) return [3 /*break*/, 3];
                        move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                        move.command = "hostSet";
                        move.newHostId = this.user.id;
                        id = "hostSet";
                        return [4 /*yield*/, this.pushMove("" + new Date().getTime() + id, move)];
                    case 1:
                        id = _a.sent();
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].getSubCollectionDocument(this.db, "session", this.session.id, "moves", id)];
                    case 2:
                        document_1 = _a.sent();
                        move = Object.assign(move, document_1.data());
                        this.executeMoves([move]);
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.getTime = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                setTimeout(function () {
                    _this.intervalMethod();
                    setTimeout(function () {
                        _this.getTime();
                    }, 1000);
                }, 0);
                return [2 /*return*/];
            });
        });
    };
    GamePage.prototype.intervalMethod = function () {
        return __awaiter(this, void 0, void 0, function () {
            var currentUser_1, offlineTime, offlineCounter, alert_1, card;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.checkForTheHost();
                        this.timeCount++;
                        if (!this.currentPlayer) return [3 /*break*/, 7];
                        if (this.bot) {
                            this.currentPlayer.timeElapse = Math.floor((new Date().getTime() - this.clientTime.getTime()) / 1000);
                        }
                        else {
                            this.currentPlayer.timeElapse = Math.round(((new Date().getTime() + this.user.timeDifference) - this.moves[this.moves.length - 1].lastUpdateTime.seconds * 1000) / 1000);
                        }
                        if (this.usersList) {
                            this.usersList.map(function (value, index) {
                                if (value.id === _this.currentPlayer.id) {
                                    currentUser_1 = value;
                                }
                            });
                        }
                        if (!(!this.bot && currentUser_1 && !currentUser_1.online && this.isHost())) return [3 /*break*/, 2];
                        offlineTime = currentUser_1.lastUpdateTime;
                        offlineCounter = Math.round(((new Date().getTime() + this.user.timeDifference) - offlineTime) / 1000);
                        if (!(offlineCounter > 30)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.playerDisconnect(this.currentPlayer.id)];
                    case 1:
                        _a.sent();
                        this.currentPlayer.myMove = true;
                        _a.label = 2;
                    case 2:
                        if (!(!this.bot && this.currentPlayer.timeElapse > 60 && this.isHost())) return [3 /*break*/, 4];
                        if (!(this.currentPlayer.id !== this.user.id)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.playerDisconnect(this.currentPlayer.id)];
                    case 3:
                        _a.sent();
                        this.currentPlayer.myMove = true;
                        _a.label = 4;
                    case 4:
                        if (!(!this.bot && this.isHost() && this.currentPlayer.id === this.user.id)) return [3 /*break*/, 6];
                        if (!(this.currentPlayer.timeElapse > 60 && this.isHost() && !this.bot)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.hostIsNotResponding(this.user.id)];
                    case 5:
                        _a.sent();
                        this.partOfGame = false;
                        this.currentPlayer.myMove = true;
                        alert_1 = this.alertCtrl.create({
                            title: "You Are Not Responding",
                            buttons: [
                                {
                                    text: "Go Offline",
                                    handler: function (data) {
                                        _this.playerDisconnect(_this.user.id);
                                        _this.storageService.resetSession();
                                        window.location.href = window.location.origin;
                                    }
                                },
                                {
                                    text: "Wait For Next Game",
                                }
                            ]
                        });
                        alert_1.present();
                        _a.label = 6;
                    case 6:
                        if (this.isHost() &&
                            this.currentPlayer &&
                            this.currentPlayer.myMove &&
                            this.currentPlayer.isBot) {
                            card = null;
                            if (this.turnCards.length == 0) {
                                card = __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */].playFirstCard(this.players, this.currentPlayer, this.bidWinner, this.trum);
                            }
                            else {
                                card = __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */].playOtherCard(this.players, this.turnCards, this.currentPlayer, this.bidWinner, this.trum);
                            }
                            if (card) {
                                card.y = -20;
                                this.cardClick(card, this.currentPlayer);
                            }
                        }
                        return [3 /*break*/, 8];
                    case 7:
                        if (this.session.waitingPlayers && this.session.waitingPlayers[0] && this.session.waitingPlayers[0].id === this.user.id) {
                            if (!this.startGame && this.timeCount < 7) {
                                this.pullOtherPlayers();
                            }
                            if (!this.startGame &&
                                (this.timeCount > 10 || this.session.waitingPlayers.length == 3)) {
                                this.startTheGame();
                            }
                        }
                        _a.label = 8;
                    case 8:
                        if (this.usersList && this.usersList.length > 0) {
                            this.maintainWaitingPlayers();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.pullOtherPlayers = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.db.collection("users").snapshotChanges()
                    .subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
                    var _i, data_1, item, dateObj, serverTime, userJoiningTime;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _i = 0, data_1 = data;
                                _a.label = 1;
                            case 1:
                                if (!(_i < data_1.length)) return [3 /*break*/, 4];
                                item = data_1[_i];
                                if (!(item.payload.doc.data()["online"] && item.payload.doc.data()["gameId"] === "")) return [3 /*break*/, 3];
                                if (!item.payload.doc.data()["lastUpdateTime"]) return [3 /*break*/, 3];
                                dateObj = new Date(item.payload.doc.data()["lastUpdateTime"].toDate());
                                serverTime = __WEBPACK_IMPORTED_MODULE_12_moment___default()(dateObj).toISOString();
                                userJoiningTime = Math.round((__WEBPACK_IMPORTED_MODULE_12_moment___default()(new Date().getTime()).diff(__WEBPACK_IMPORTED_MODULE_12_moment___default()(serverTime))) / 1000);
                                if (!(userJoiningTime < 6)) return [3 /*break*/, 3];
                                return [4 /*yield*/, this.db.collection("users").doc(item.payload.doc.data()["id"]).set({ gameId: this.session.id }, { merge: true })];
                            case 2:
                                _a.sent();
                                _a.label = 3;
                            case 3:
                                _i++;
                                return [3 /*break*/, 1];
                            case 4:
                                ;
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    GamePage.prototype.playersPoints = function (players) {
        return __awaiter(this, void 0, void 0, function () {
            var playerPoints, basePoint, _i, players_1, item, playerDoc, playerData, playerPoint, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        playerPoints = [];
                        _i = 0, players_1 = players;
                        _a.label = 1;
                    case 1:
                        if (!(_i < players_1.length)) return [3 /*break*/, 6];
                        item = players_1[_i];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].getDocument(this.db, "users", item.id)];
                    case 3:
                        playerDoc = _a.sent();
                        playerData = playerDoc.data();
                        playerPoint = playerData.points;
                        playerPoints.push(playerPoint);
                        return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 5];
                    case 5:
                        _i++;
                        return [3 /*break*/, 1];
                    case 6:
                        playerPoints.map(function (value, index) {
                            if (index === 0) {
                                basePoint = value;
                            }
                            if (index !== 0 && value < basePoint) {
                                basePoint = value;
                            }
                        });
                        this.basePointForGame = Math.ceil(basePoint * 0.1);
                        this.botPointForGame = basePoint / 2;
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.deductPoints = function (players, basePoint) {
        return __awaiter(this, void 0, void 0, function () {
            var deductAmmount;
            var _this = this;
            return __generator(this, function (_a) {
                deductAmmount = Math.ceil(basePoint);
                console.log(deductAmmount);
                players.map(function (value, index) { return __awaiter(_this, void 0, void 0, function () {
                    var playerDoc, previousPoints, updatedPoints;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].get(this.db, "users", value.id)];
                            case 1:
                                playerDoc = _a.sent();
                                previousPoints = playerDoc.data().points;
                                updatedPoints = previousPoints - deductAmmount;
                                return [4 /*yield*/, this.db.collection("users").doc(value.id).set({ points: updatedPoints }, { merge: true })];
                            case 2:
                                _a.sent();
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    GamePage.prototype.playerPoint = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var playerDoc, playerData, playerPoint;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].getDocument(this.db, "users", id)];
                    case 1:
                        playerDoc = _a.sent();
                        playerData = playerDoc.data();
                        playerPoint = playerData.points;
                        return [2 /*return*/, playerPoint];
                }
            });
        });
    };
    GamePage.prototype.startTheGame = function () {
        return __awaiter(this, void 0, void 0, function () {
            var players, realPlayers, ct, _i, _a, item, player, _b, botPlayer1, botPlayer2, botPlayer1, cards, i, move, id;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        this.alreadyStarted = false;
                        this.startGame = true;
                        players = [];
                        realPlayers = [];
                        ct = 0;
                        _i = 0, _a = this.session.waitingPlayers;
                        _c.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        item = _a[_i];
                        player = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                        player.id = item.id;
                        player.name = item.name;
                        player.index = ct;
                        player.isBot = item.isBot;
                        _b = player;
                        return [4 /*yield*/, this.playerPoint(item.id)];
                    case 2:
                        _b.points = _c.sent();
                        if (this.user.id === item.id) {
                            player.isHost = true;
                        }
                        else {
                            player.isHost = false;
                        }
                        players.push(player);
                        realPlayers.push(player);
                        ct++;
                        if (ct == 4) {
                            return [3 /*break*/, 4];
                        }
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [4 /*yield*/, this.playersPoints(realPlayers)];
                    case 5:
                        _c.sent();
                        return [4 /*yield*/, this.deductPoints(realPlayers, this.basePointForGame)];
                    case 6:
                        _c.sent();
                        if (players.length == 1) {
                            botPlayer1 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                            botPlayer1.id = "1";
                            botPlayer1.name = __WEBPACK_IMPORTED_MODULE_17_random_name___default.a.first();
                            botPlayer1.points = Math.ceil(this.botPointForGame);
                            botPlayer1.isBot = true;
                            botPlayer1.index = 1;
                            botPlayer1.bidCount = 0;
                            players.push(botPlayer1);
                            botPlayer2 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                            botPlayer2.id = "2";
                            botPlayer2.name = __WEBPACK_IMPORTED_MODULE_17_random_name___default.a.first();
                            botPlayer2.isBot = true;
                            botPlayer2.index = 2;
                            botPlayer2.bidCount = 0;
                            botPlayer2.points = Math.ceil(this.botPointForGame * 0.8);
                            players.push(botPlayer2);
                        }
                        else if (players.length == 2) {
                            botPlayer1 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                            botPlayer1.id = "2";
                            botPlayer1.name = __WEBPACK_IMPORTED_MODULE_17_random_name___default.a.first();
                            botPlayer1.isBot = true;
                            botPlayer1.index = 2;
                            botPlayer1.bidCount = 0;
                            botPlayer1.points = Math.ceil(this.botPointForGame);
                            players.push(botPlayer1);
                        }
                        cards = __WEBPACK_IMPORTED_MODULE_6__models_card__["a" /* Card */].deck();
                        this.deck = new __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */](cards, players, this.board.offsetWidth, this.board.offsetHeight);
                        for (i = 0; i < this.session.level; i++) {
                            this.deck.shuffleCards();
                        }
                        this.deck.distributeCards();
                        move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                        move.hostId = this.user.id;
                        move.command = "distribute";
                        move.basePointForGame = this.basePointForGame;
                        move.data = {};
                        move.data.players = this.deck.players;
                        id = "distribute";
                        this.pushMove("" + (new Date().getTime() + this.user.timeDifference) + id, move);
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.startGameInChildSession = function () {
        return __awaiter(this, void 0, void 0, function () {
            var players, realPlayers, ct, _i, _a, item, player, _b, botPlayer1, botPlayer2, botPlayer1, cards, i, move;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        players = [];
                        realPlayers = [];
                        ct = 0;
                        _i = 0, _a = this.session.waitingPlayers;
                        _c.label = 1;
                    case 1:
                        if (!(_i < _a.length)) return [3 /*break*/, 4];
                        item = _a[_i];
                        player = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                        player.id = item.id;
                        player.name = item.name;
                        player.index = ct;
                        player.isBot = item.isBot;
                        _b = player;
                        return [4 /*yield*/, this.playerPoint(item.id)];
                    case 2:
                        _b.points = _c.sent();
                        if (this.user.id === item.id) {
                            player.isHost = true;
                        }
                        players.push(player);
                        realPlayers.push(player);
                        ct++;
                        if (ct == 4) {
                            return [3 /*break*/, 4];
                        }
                        _c.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [4 /*yield*/, this.playersPoints(realPlayers)];
                    case 5:
                        _c.sent();
                        return [4 /*yield*/, this.deductPoints(realPlayers, this.basePointForGame)];
                    case 6:
                        _c.sent();
                        if (players.length == 1) {
                            botPlayer1 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                            botPlayer1.id = "1";
                            botPlayer1.name = __WEBPACK_IMPORTED_MODULE_17_random_name___default.a.first();
                            botPlayer1.points = this.botPointForGame;
                            botPlayer1.isBot = true;
                            botPlayer1.index = 1;
                            botPlayer1.bidCount = 0;
                            players.push(botPlayer1);
                            botPlayer2 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                            botPlayer2.id = "2";
                            botPlayer2.name = __WEBPACK_IMPORTED_MODULE_17_random_name___default.a.first();
                            botPlayer2.points = this.botPointForGame * 0.8;
                            botPlayer2.isBot = true;
                            botPlayer2.index = 2;
                            botPlayer2.bidCount = 0;
                            players.push(botPlayer2);
                        }
                        else if (players.length == 2) {
                            botPlayer1 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                            botPlayer1.id = "2";
                            botPlayer1.name = __WEBPACK_IMPORTED_MODULE_17_random_name___default.a.first();
                            botPlayer1.points = this.botPointForGame;
                            botPlayer1.isBot = true;
                            botPlayer1.index = 2;
                            botPlayer1.bidCount = 0;
                            players.push(botPlayer1);
                        }
                        cards = __WEBPACK_IMPORTED_MODULE_6__models_card__["a" /* Card */].deck();
                        this.childSessionDeck = new __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */](cards, players, this.board.offsetWidth, this.board.offsetHeight);
                        for (i = 0; i < this.session.level; i++) {
                            this.childSessionDeck.shuffleCards();
                        }
                        this.childSessionDeck.distributeCards();
                        move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                        move.hostId = this.user.id;
                        move.command = "distribute";
                        move.basePointForGame = this.basePointForGame;
                        move.data = {};
                        move.data.players = this.childSessionDeck.players;
                        return [4 /*yield*/, this.pushDistributeMoveInChildSession("distribute", move)];
                    case 7:
                        _c.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.openBid = function () {
        var _this = this;
        var playerBidCount = !this.players[0].bidCount;
        playerBidCount ? 0
            : this.players[0].bidCount;
        var alert = this.alertCtrl.create({
            title: "Bidding",
            inputs: [
                {
                    name: "bidCount",
                    placeholder: "Enter number of hands you can make",
                    type: "number",
                    value: ""
                }
            ],
            buttons: [
                {
                    text: "Submit Bid",
                    handler: function (data) {
                        var move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                        move.command = "bid";
                        move.data = {};
                        move.data.playerIndex = _this.players[0].index;
                        move.data.hands = data.bidCount;
                        var id = "bid" + _this.players[0].index;
                        _this.pushMove("" + (new Date().getTime() + _this.user.timeDifference) + id, move);
                        _this.showBidButton = false;
                    }
                }
            ]
        });
        alert.present();
    };
    GamePage.prototype.bid = function (move) {
        var player = this.getPlayerByIndex(move.data.playerIndex);
        var count = parseInt(move.data.hands);
        if (move.lastUpdateTime && count != player.bidCount) {
            player.bidCount = count;
            var dateObj = new Date(move.lastUpdateTime.toDate());
            player.bidTime = __WEBPACK_IMPORTED_MODULE_12_moment___default()(dateObj).toISOString();
        }
    };
    GamePage.prototype.getPlayerByIndex = function (index) {
        return this.players.filter(function (x) { return x.index == index; })[0];
    };
    GamePage.prototype.bidFinished = function (move) {
        this.biddingDone = true;
        this.bidWinner = this.getPlayerByIndex(move.data.bidWinnerIndex);
        this.marId = this.bidWinner.id;
        this.marName = this.bidWinner.name;
        this.marCount = move.data.mar;
    };
    GamePage.prototype.setTrum = function (move) {
        var pl = this.getPlayerByIndex(move.data.index);
        var card = pl.playerCards.filter(function (x) { return x.name === move.data.card; })[0];
        var turn = new __WEBPACK_IMPORTED_MODULE_13__models_turn__["a" /* Turn */]();
        turn.card = card;
        turn.player = pl;
        this.turnCards.push(turn);
        this.playerClick(card, pl);
        this.currentPlayer = this.bidWinner;
        this.firstTurn = false;
    };
    GamePage.prototype.trumset = function (move) {
        this.trum = move.data.trum;
    };
    GamePage.prototype.selectTrum = function () {
        var _this = this;
        if (this.user.id === this.bidWinner.id) {
            var prompt_1 = this.alertCtrl.create({
                title: "Set trum",
                message: "Select Trum",
                inputs: [
                    {
                        type: "radio",
                        label: "♥ Hearts",
                        value: "H"
                    },
                    {
                        type: "radio",
                        label: "♠ Spades",
                        value: "S"
                    },
                    {
                        type: "radio",
                        label: "♦ Diamond",
                        value: "D"
                    },
                    {
                        type: "radio",
                        label: "♣ Club",
                        value: "C"
                    }
                ],
                buttons: [
                    {
                        text: "set",
                        handler: function (data) {
                            var move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                            move.command = "trumset";
                            move.data = {};
                            move.data.trum = data;
                            var turn = _this.turnCards[_this.turnCards.length - 1];
                            move.turn = __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */].arrageById(turn.player.id, _this.players)[1].index;
                            var id = "trumset";
                            _this.pushMove("" + (new Date().getTime() + _this.user.timeDifference) + id, move);
                        }
                    }
                ]
            });
            prompt_1.present();
        }
    };
    GamePage.prototype.getTrumSymbol = function () {
        if (this.trum == "S") {
            return "♠";
        }
        else if (this.trum == "C") {
            return "♣";
        }
        else if (this.trum == "D") {
            return "♦";
        }
        else if (this.trum == "H") {
            return "♥";
        }
        else {
            return "";
        }
    };
    GamePage.prototype.canSetTrum = function () {
        if (this.partOfGame && this.isBidWinner() && this.command == "settrum") {
            return true;
        }
        else {
            return false;
        }
    };
    GamePage.prototype.move = function (move) {
        var _this = this;
        return new Promise(function (resolve) {
            var pl = _this.getPlayerByIndex(move.data.index);
            var card = pl.playerCards.filter(function (x) { return x.name === move.data.card; })[0];
            var turn = new __WEBPACK_IMPORTED_MODULE_13__models_turn__["a" /* Turn */]();
            turn.card = card;
            turn.player = pl;
            _this.turnCards.push(turn);
            _this.playerClick(card, pl);
            if (move.complete) {
                setTimeout(function () {
                    var hand = new __WEBPACK_IMPORTED_MODULE_14__models_hand__["a" /* Hand */]();
                    hand.turns = [];
                    var seriesFinshed = _this.turnCards.filter(function (x) { return x.card.type != _this.turnCards[0].card.type; });
                    for (var _i = 0, seriesFinshed_1 = seriesFinshed; _i < seriesFinshed_1.length; _i++) {
                        var item = seriesFinshed_1[_i];
                        if (item.player.seriesUsed.indexOf(_this.turnCards[0].card.type) > -1) {
                            item.player.seriesUsed.push(_this.turnCards[0].card.type);
                        }
                    }
                    for (var _a = 0, _b = _this.turnCards; _a < _b.length; _a++) {
                        var item = _b[_a];
                        hand.turns.push(item);
                        var cardIndex = item.player.playerCards.indexOf(item.card);
                        item.player.playerCards.splice(cardIndex, 1);
                        var index = item.player.cards.indexOf(item.card.name);
                        item.player.cards.splice(index, 1);
                    }
                    _this.turnCards.length = 0;
                    hand.winnerIndex = move.winnerIndex;
                    _this.hands.push(hand);
                    __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */].arrangeCards(_this.players[0], _this.board.offsetWidth, true, 0, 100);
                    resolve(hand);
                }, 2000);
            }
            else {
                resolve(turn);
            }
            _this.firstTurn = false;
        });
    };
    GamePage.prototype.isHost = function () {
        if (this.bot) {
            return true;
        }
        else {
            if (this.session) {
                if (this.session.waitingPlayers.length > 0) {
                    if (this.session.waitingPlayers[0].id === this.user.id) {
                        return this.session.waitingPlayers[0].isHost;
                    }
                }
            }
            else {
                return false;
            }
        }
    };
    GamePage.prototype.setChatBoxSize = function (width, height) {
        this.chatBoxWidth = width - 120 + "px";
        this.chatBoxHeight = height - 260 + "px";
        this.cardsTop = height - 110 + "px";
        if (!this.bot) {
            this.cardsTop = height - 110 - 43 + "px";
        }
    };
    GamePage.prototype.removeUser = function (playerId) {
        this.db.collection("users").doc(playerId).set({ gameId: "" }, { merge: true });
    };
    GamePage.prototype.calcTimeDifference = function () {
        var _this = this;
        this.user["lastUpdateTimeClient"] = __WEBPACK_IMPORTED_MODULE_12_moment___default()(new Date()).utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
        __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].setWithCallback(this.db, "users", this.user, this.user.id).then(function () {
            __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].get(_this.db, "users", _this.user.id).then(function (data) {
                var userdto = data.data();
                _this.user = Object.assign(userdto, _this.user);
                _this.user.id = data.id;
                _this.user.points = userdto.points;
                var clientTime = userdto["lastUpdateTimeClient"];
                if (userdto["lastUpdateTime"]) {
                    var dateObj = new Date(userdto["lastUpdateTime"].toDate()); //error
                    var serverTime = __WEBPACK_IMPORTED_MODULE_12_moment___default()(dateObj).toISOString();
                    _this.user.timeDifference = Math.round((__WEBPACK_IMPORTED_MODULE_12_moment___default()(clientTime).diff(__WEBPACK_IMPORTED_MODULE_12_moment___default()(serverTime))) / 1000);
                    console.log(_this.user.timeDifference);
                }
            });
        });
    };
    GamePage.prototype.addPlayerOnReconnect = function (Session, userId) {
        return __awaiter(this, void 0, void 0, function () {
            var player, move, id;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        player = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                        player.isBooked = true;
                        player.name = this.user.name;
                        player.id = this.user.id;
                        player.command = "joined";
                        move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                        move.data = player;
                        move.command = "joined";
                        id = "playerJoined";
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].subCol(this.db, Session, move, "" + new Date().getTime() + id)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.playOffline = function () {
        var _this = this;
        localStorage.setItem("sessionId", null);
        this.bot = true;
        this.players.map(function (value) {
            if (value.id !== _this.user.id) {
                if (!value.isBot) {
                    value.isBot = true;
                }
            }
        });
    };
    GamePage.prototype.browserConnect = function () {
        var _this = this;
        if (!this.bot) {
            window.addEventListener('offline', function (e) {
                var alert = _this.alertCtrl.create({
                    title: "You Are Offline",
                    subTitle: "Check Your Internet",
                    buttons: [
                        {
                            text: "Play Offline",
                            handler: function () {
                                _this.playOffline();
                            }
                        },
                        {
                            text: "Wait For Connection",
                            handler: function () {
                                console.log("Waiting ...");
                            }
                        }
                    ]
                });
                alert.present();
            });
        }
        window.addEventListener('online', function (e) { return __awaiter(_this, void 0, void 0, function () {
            var alreadyAdded_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.bot) {
                            this.removeUser(this.user.id);
                        }
                        if (!!this.bot) return [3 /*break*/, 2];
                        this.gameSubscription = __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].subscribeCollectionSnapshot(this.db, this.sessionId).subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
                            var moves, _i, data_2, item, obj, move;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        moves = [];
                                        if (data.length) {
                                            this.moveCount = data.length;
                                        }
                                        for (_i = 0, data_2 = data; _i < data_2.length; _i++) {
                                            item = data_2[_i];
                                            if (item.payload.doc.data()["lastUpdateTime"] &&
                                                this.moveIds.indexOf(item.payload.doc.id) == -1) {
                                                this.moveIds.push(item.payload.doc.id);
                                                obj = item.payload.doc.data();
                                                if (!obj["command"]) {
                                                    this.addMessage(obj);
                                                }
                                                else {
                                                    move = Object.assign(new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */](), obj);
                                                    move.id = item.payload.doc.id;
                                                    moves.push(move);
                                                }
                                            }
                                        }
                                        if (!(moves && moves.length)) return [3 /*break*/, 2];
                                        this.moves = moves;
                                        return [4 /*yield*/, this.executeMoves(this.moves)];
                                    case 1:
                                        _a.sent();
                                        _a.label = 2;
                                    case 2: return [2 /*return*/];
                                }
                            });
                        }); });
                        alreadyAdded_1 = false;
                        return [4 /*yield*/, this.session.waitingPlayers.map(function (value) {
                                if (value.id === _this.user.id) {
                                    alreadyAdded_1 = true;
                                }
                                ;
                            })];
                    case 1:
                        _a.sent();
                        !alreadyAdded_1 && this.addPlayerOnReconnect(this.session.id, this.user.id);
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); });
    };
    ;
    GamePage.prototype.executeAllMoves = function () {
        var _this = this;
        if (!this.bot) {
            this.gameSubscription = __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].subscribeCollectionSnapshot(this.db, this.sessionId).subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
                var moves, _i, data_3, item, obj, move;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            moves = [];
                            if (data.length) {
                                this.moveCount = data.length;
                            }
                            for (_i = 0, data_3 = data; _i < data_3.length; _i++) {
                                item = data_3[_i];
                                if (item.payload.doc.data()["lastUpdateTime"] &&
                                    this.moveIds.indexOf(item.payload.doc.id) == -1) {
                                    this.moveIds.push(item.payload.doc.id);
                                    obj = item.payload.doc.data();
                                    if (!obj["command"]) {
                                        this.addMessage(obj);
                                    }
                                    else {
                                        move = Object.assign(new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */](), obj);
                                        move.id = item.payload.doc.id;
                                        moves.push(move);
                                    }
                                }
                            }
                            if (!(moves && moves.length)) return [3 /*break*/, 2];
                            this.moves = moves;
                            return [4 /*yield*/, this.executeMoves(this.moves)];
                        case 1:
                            _a.sent();
                            _a.label = 2;
                        case 2: return [2 /*return*/];
                    }
                });
            }); });
        }
    };
    GamePage.prototype.ngOnInit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var search, obj, cards, players, _i, _a, item, player_1, player, i, move;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        // await this.executeAllMoves();
                        //  if(document.documentElement.requestFullscreen)
                        //  document.querySelector(".gameScreen").requestFullscreen();
                        //  console.log(document.querySelector(".gameScreen").requestFullscreen());
                        //
                        // else if(document.documentElement.webkitRequestFullScreen)
                        // 	document.querySelector("#container").webkitRequestFullScreen();
                        // screen.orientation.lock("landscape-primary")
                        //   .then(function() {
                        //       screen.orientation.unlock();
                        //   })
                        // 	.catch(function(error) {
                        // 		alert(error);
                        //   });
                        //   document.addEventListener("orientationchange", function(event){
                        //     switch(window.orientation)
                        //     {
                        //         case -90: case 90:
                        //             /* Device is in landscape mode */
                        //             break;
                        //         default:
                        //             /* Device is in portrait mode */
                        //     }
                        // });
                        //   else if(document.querySelector("#container").webkitRequestFullScreen)
                        // document.querySelector("#container").webkitRequestFullScreen();
                        this.browserConnect();
                        this.calcTimeDifference();
                        this.listenOnlineStatus();
                        search = window.location.search;
                        if (!window.location.search) return [3 /*break*/, 2];
                        search = search.substring(1, search.length);
                        obj = JSON.parse('{"' + decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
                        if (!(obj.sid && !this.navParams.get("isQuit"))) return [3 /*break*/, 2];
                        console.log(obj.sid);
                        return [4 /*yield*/, this.storageService.setSession(obj.sid)];
                    case 1:
                        _b.sent();
                        _b.label = 2;
                    case 2:
                        this.board = this.game.nativeElement;
                        this.setChatBoxSize(this.board.offsetWidth, this.board.offsetHeight);
                        this.events.subscribe("exit-game", function (data) {
                            _this.storageService.resetSession();
                            window.location.reload();
                        });
                        if (this.bot) {
                            cards = __WEBPACK_IMPORTED_MODULE_6__models_card__["a" /* Card */].deck();
                            players = [];
                            for (_i = 0, _a = this.session.players; _i < _a.length; _i++) {
                                item = _a[_i];
                                player_1 = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                                player_1.id = item.id;
                                player_1.name = item.name;
                                player_1.index = item.index;
                                player_1.isBot = item.isBot;
                                if ((this.bot && player_1.index > 0) || player_1.isBot) {
                                    player_1.bidCount = 0;
                                    player_1.bidTime = this.session.lastUpdateTime;
                                }
                                if (player_1.id == this.user.id) {
                                    this.currentPlayer = player_1;
                                }
                                players.push(player_1);
                            }
                            player = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                            player.id = "4";
                            player.name = "Ashfaq";
                            player.index = 3;
                            player.isBot = true;
                            player.bidCount = 0;
                            players.push(player);
                            this.deck = new __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */](cards, players, this.board.offsetWidth, this.board.offsetHeight);
                            for (i = 0; i < this.session.level; i++) {
                                this.deck.shuffleCards();
                            }
                            this.deck.distributeCards();
                            move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                            move.command = "distribute";
                            move.data = {};
                            move.data.players = this.deck.players;
                            //this.updateSession();
                            this.pushMove("distribute", move);
                        }
                        if (this.session) {
                            this.getTime();
                            if (!this.bot) {
                                if (this.session.sessionCount === 0 && this.session.activeSession !== this.session.id) {
                                    window.location.href = window.location.origin + "?sid=" + this.session.activeSession;
                                }
                                this.gameSubscription = __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].subscribeCollectionSnapshot(this.db, this.sessionId).subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
                                    var moves, _i, data_4, item, obj, move;
                                    return __generator(this, function (_a) {
                                        moves = [];
                                        if (data.length) {
                                            this.moveCount = data.length;
                                        }
                                        for (_i = 0, data_4 = data; _i < data_4.length; _i++) {
                                            item = data_4[_i];
                                            if (item.payload.doc.data()["lastUpdateTime"] &&
                                                this.moveIds.indexOf(item.payload.doc.id) == -1) {
                                                this.moveIds.push(item.payload.doc.id);
                                                obj = item.payload.doc.data();
                                                if (!obj["command"]) {
                                                    this.addMessage(obj);
                                                }
                                                else {
                                                    move = Object.assign(new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */](), obj);
                                                    move.id = item.payload.doc.id;
                                                    moves.push(move);
                                                }
                                            }
                                        }
                                        if (moves && moves.length) {
                                            this.moves = moves;
                                            console.log(moves);
                                            this.executeMoves(moves);
                                        }
                                        return [2 /*return*/];
                                    });
                                }); });
                            }
                        }
                        else {
                            this.navCtrl.pop();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.addMessage = function (msg) {
        var exists = false;
        if (msg.userId == this.user.id) {
            var array = this.messages.filter(function (x) { return x.id == msg.id; });
            if (array.length > 0) {
                exists = true;
                array[0] = Object.assign(array[0], msg);
            }
        }
        if (!exists) {
            var message = new __WEBPACK_IMPORTED_MODULE_16__models_message__["a" /* Message */]();
            message = Object.assign(message, msg);
            this.messages.push(message);
            this.scrollToBottom();
        }
    };
    GamePage.prototype.executeMoves = function (moves) {
        return __awaiter(this, void 0, void 0, function () {
            var turnTemp, _loop_1, this_1, _i, moves_1, move;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        turnTemp = -1;
                        _loop_1 = function (move) {
                            var dateObj, alreadyAdded_2, waitingPlayer, cards, data, hostAlreadyExist_1, alert_2;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        dateObj = move.lastUpdateTime && new Date(move.lastUpdateTime.toDate());
                                        this_1.lastMoveTime = __WEBPACK_IMPORTED_MODULE_12_moment___default()(dateObj).toISOString();
                                        this_1.clientTime = new Date();
                                        if (move.mar >= 0 && move.complete) {
                                            this_1.mar = move.mar;
                                        }
                                        if (move.turn >= 0) {
                                            turnTemp = move.turn;
                                        }
                                        else {
                                            turnTemp = -1;
                                        }
                                        this_1.command = move.command;
                                        if (!(move.command === "joined")) return [3 /*break*/, 1];
                                        alreadyAdded_2 = false;
                                        this_1.session.waitingPlayers.map(function (value) {
                                            if (value.id === move.data.id) {
                                                alreadyAdded_2 = true;
                                            }
                                            ;
                                        });
                                        if (!alreadyAdded_2) {
                                            waitingPlayer = new __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */]();
                                            waitingPlayer.id = move.data.id;
                                            waitingPlayer.name = move.data.name;
                                            waitingPlayer.isBooked = move.data.isBooked;
                                            waitingPlayer.points = move.data.points;
                                            this_1.session.waitingPlayers.push(waitingPlayer);
                                        }
                                        ;
                                        return [3 /*break*/, 14];
                                    case 1:
                                        if (!(move.command === "distribute" && this_1.arrangeCards == false)) return [3 /*break*/, 2];
                                        if (!this_1.bot) {
                                            this_1.db.collection("users").doc(this_1.user.id).set({ gameId: this_1.session.id }, { merge: true });
                                        }
                                        this_1.distribute(move);
                                        this_1.startGame = true;
                                        this_1.basePointForGame = move.basePointForGame;
                                        if (this_1.alreadyStarted) {
                                            cards = __WEBPACK_IMPORTED_MODULE_6__models_card__["a" /* Card */].deck();
                                            this_1.deck = new __WEBPACK_IMPORTED_MODULE_5__models_deck__["a" /* Deck */](cards, this_1.players, this_1.board.offsetWidth, this_1.board.offsetHeight);
                                        }
                                        return [3 /*break*/, 14];
                                    case 2:
                                        if (!(move.command === "bid")) return [3 /*break*/, 3];
                                        this_1.bid(move);
                                        return [3 /*break*/, 14];
                                    case 3:
                                        if (!(move.command === "bidDone")) return [3 /*break*/, 4];
                                        this_1.bidFinished(move);
                                        return [3 /*break*/, 14];
                                    case 4:
                                        if (!(move.command === "settrum" && this_1.firstTurn == true)) return [3 /*break*/, 5];
                                        this_1.setTrum(move);
                                        return [3 /*break*/, 14];
                                    case 5:
                                        if (!(move.command === "trumset")) return [3 /*break*/, 6];
                                        this_1.trumset(move);
                                        return [3 /*break*/, 14];
                                    case 6:
                                        if (!(move.command === "move")) return [3 /*break*/, 8];
                                        return [4 /*yield*/, this_1.move(move)];
                                    case 7:
                                        data = _a.sent();
                                        console.log(data);
                                        return [3 /*break*/, 14];
                                    case 8:
                                        if (!(move.command === "quit")) return [3 /*break*/, 9];
                                        this_1.session.waitingPlayers.map(function (value, index) {
                                            if (value.id === move.quitId) {
                                                _this.session.waitingPlayers.splice(index, 1);
                                            }
                                            ;
                                        });
                                        this_1.players.map(function (value, index) {
                                            if (value.id === move.quitId) {
                                                _this.players[index].isBot = true;
                                                _this.players[index].name = "Bot";
                                                _this.players[index].id = "123456789";
                                            }
                                            ;
                                        });
                                        if (this_1.isHost()) {
                                            this_1.removeUser(move.quitId);
                                        }
                                        return [3 /*break*/, 14];
                                    case 9:
                                        if (!(move.command === "disconnect")) return [3 /*break*/, 10];
                                        if (this_1.user.id === move.disconnectId) {
                                            this_1.partOfGame = false;
                                        }
                                        this_1.session.waitingPlayers.map(function (value, index) {
                                            if (value.id === move.disconnectId) {
                                                //change
                                                _this.session.waitingPlayers.splice(index, 1);
                                            }
                                            ;
                                        });
                                        this_1.players.map(function (value, index) {
                                            if (value.id === move.disconnectId) {
                                                _this.players[index].isBot = true;
                                                _this.players[index].name = "Bot";
                                                _this.players[index].id = "123456789";
                                            }
                                            ;
                                        });
                                        return [3 /*break*/, 14];
                                    case 10:
                                        if (!(move.command === "hostIsNotResponding")) return [3 /*break*/, 11];
                                        this_1.players.map(function (value, index) {
                                            if (value.id === move.disconnectId) {
                                                _this.players[index].isBot = true;
                                                _this.players[index].name = "Bot";
                                                _this.players[index].id = "123456789";
                                            }
                                            ;
                                        });
                                        return [3 /*break*/, 14];
                                    case 11:
                                        if (!(move.command === "hostSet")) return [3 /*break*/, 12];
                                        this_1.session.waitingPlayers.map(function (value, index) {
                                            if (value.isHost) {
                                                hostAlreadyExist_1 = true;
                                            }
                                        });
                                        if (!hostAlreadyExist_1) {
                                            this_1.session.waitingPlayers.map(function (value, index) {
                                                if (value.id === move.newHostId) {
                                                    _this.session.waitingPlayers[index].isHost = true;
                                                }
                                            });
                                        }
                                        return [3 /*break*/, 14];
                                    case 12:
                                        if (!(move.command === "joinNewSession")) return [3 /*break*/, 14];
                                        return [4 /*yield*/, this_1.db.collection("session").doc(this_1.session.parentSession).set({ activeSession: this_1.childSession }, { merge: true })];
                                    case 13:
                                        _a.sent();
                                        window.location.href = this_1.getChildSessionUrl(move.childSessionId);
                                        _a.label = 14;
                                    case 14:
                                        if (this_1.mar > 0 &&
                                            this_1.mar === this_1.marCount &&
                                            this_1.gameWon === false) {
                                            this_1.gameWon = true;
                                        }
                                        if (move.gameOver) {
                                            this_1.gameOver = true;
                                            alert_2 = this_1.alertCtrl.create({
                                                title: "Game Over",
                                                subTitle: this_1.gameWon
                                                    ? this_1.marName + " won the game"
                                                    : this_1.marName + " lost the game",
                                                buttons: ["OK"]
                                            });
                                            alert_2.present();
                                        }
                                        if (!move.gameOver) return [3 /*break*/, 17];
                                        if (!this_1.isHost()) return [3 /*break*/, 17];
                                        return [4 /*yield*/, this_1.updatePoints()];
                                    case 15:
                                        _a.sent();
                                        return [4 /*yield*/, this_1.newGame()];
                                    case 16:
                                        _a.sent();
                                        _a.label = 17;
                                    case 17: return [2 /*return*/];
                                }
                            });
                        };
                        this_1 = this;
                        _i = 0, moves_1 = moves;
                        _a.label = 1;
                    case 1:
                        if (!(_i < moves_1.length)) return [3 /*break*/, 4];
                        move = moves_1[_i];
                        return [5 /*yield**/, _loop_1(move)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4:
                        this.turnIndex = turnTemp;
                        if (this.turnIndex >= 0) {
                            this.currentPlayer = this.getPlayerByIndex(this.turnIndex);
                            this.currentPlayer.timeElapse = 0;
                            this.currentPlayer.myMove = true;
                        }
                        if (!this.isBiddingComplete && this.players.length > 0) {
                            this.isBiddingComplete =
                                this.players.filter(function (x) { return x.bidCount >= 0; }).length == this.players.length;
                        }
                        if (this.isBiddingComplete && this.isHost() && !this.biddingDone) {
                            this.closeBid();
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.playerQuit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var move, id;
            return __generator(this, function (_a) {
                move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                move.command = "quit";
                move.quitId = this.user.id;
                id = "playerQuit";
                this.pushMove("" + (new Date().getTime() + this.user.timeDifference + this.user.timeDifference) + id, move);
                this.removeUser(this.user.id);
                return [2 /*return*/];
            });
        });
    };
    GamePage.prototype.playerDisconnect = function (playerId) {
        return __awaiter(this, void 0, void 0, function () {
            var move, id, document;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.removeUser(playerId);
                        move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                        move.command = "disconnect";
                        move.disconnectId = playerId;
                        id = "playerDisconnect";
                        return [4 /*yield*/, this.pushMove("" + new Date().getTime() + id, move)];
                    case 1:
                        id = _a.sent();
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].getSubCollectionDocument(this.db, "session", this.session.id, "moves", id)];
                    case 2:
                        document = _a.sent();
                        move = Object.assign(move, document.data());
                        this.executeMoves([move]);
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.hostIsNotResponding = function (hostId) {
        return __awaiter(this, void 0, void 0, function () {
            var move, id, document;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                        move.command = "hostIsNotResponding";
                        move.disconnectId = hostId;
                        id = "hostIsNotResponding";
                        return [4 /*yield*/, this.pushMove("" + (new Date().getTime() + this.user.timeDifference) + id, move)];
                    case 1:
                        id = _a.sent();
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].getSubCollectionDocument(this.db, "session", this.session.id, "moves", id)];
                    case 2:
                        document = _a.sent();
                        move = Object.assign(move, document.data());
                        this.executeMoves([move]);
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.getChildSessionUrl = function (childSessionId) {
        if (!this.isHost()) {
            this.childSession = childSessionId;
        }
        return window.location.origin + "?sid=" + this.childSession;
    };
    GamePage.prototype.updatePoints = function () {
        return __awaiter(this, void 0, void 0, function () {
            var playerDoc, previousPoints, updatedPoints;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.gameWon) return [3 /*break*/, 3];
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].getDocument(this.db, "users", this.marId)];
                    case 1:
                        playerDoc = _a.sent();
                        previousPoints = playerDoc.data().points;
                        updatedPoints = previousPoints + (this.basePointForGame * 3);
                        return [4 /*yield*/, this.db.collection("users").doc(this.marId).set({ points: updatedPoints }, { merge: true })];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        this.players.map(function (value, index) { return __awaiter(_this, void 0, void 0, function () {
                            var playerDoc, previousPoints, updatedPoints, playerDoc, previousPoints, updatedPoints;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!(value.id !== this.marId && !value.isBot)) return [3 /*break*/, 3];
                                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].getDocument(this.db, "users", value.id)];
                                    case 1:
                                        playerDoc = _a.sent();
                                        previousPoints = playerDoc.data().points;
                                        updatedPoints = previousPoints + (this.basePointForGame * 2);
                                        return [4 /*yield*/, this.db.collection("users").doc(value.id).set({ points: updatedPoints }, { merge: true })];
                                    case 2:
                                        _a.sent();
                                        _a.label = 3;
                                    case 3:
                                        if (!(value.id === this.marId && !value.isBot)) return [3 /*break*/, 6];
                                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].getDocument(this.db, "users", value.id)];
                                    case 4:
                                        playerDoc = _a.sent();
                                        previousPoints = playerDoc.data().points;
                                        updatedPoints = previousPoints - (this.basePointForGame * 2);
                                        return [4 /*yield*/, this.db.collection("users").doc(value.id).set({ points: updatedPoints }, { merge: true })];
                                    case 5:
                                        _a.sent();
                                        _a.label = 6;
                                    case 6: return [2 /*return*/];
                                }
                            });
                        }); });
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.listenOnlineStatus = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.db.collection("users").snapshotChanges()
                    .subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
                    var users, _i, data_5, item, obj, user, obj, user;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                users = [];
                                _i = 0, data_5 = data;
                                _a.label = 1;
                            case 1:
                                if (!(_i < data_5.length)) return [3 /*break*/, 5];
                                item = data_5[_i];
                                if (!(item.payload.doc.data()["id"] === this.user.id)) return [3 /*break*/, 3];
                                return [4 /*yield*/, item.payload.doc.data()];
                            case 2:
                                obj = _a.sent();
                                user = Object.assign(new __WEBPACK_IMPORTED_MODULE_11__models_user__["a" /* User */](), obj);
                                this.user.points = user.points;
                                _a.label = 3;
                            case 3:
                                ;
                                if (item.payload.doc.data()["gameId"] === this.session.id) {
                                    obj = item.payload.doc.data();
                                    user = Object.assign(new __WEBPACK_IMPORTED_MODULE_11__models_user__["a" /* User */](), obj);
                                    users.push(user);
                                }
                                _a.label = 4;
                            case 4:
                                _i++;
                                return [3 /*break*/, 1];
                            case 5:
                                ;
                                if (users && users.length) {
                                    this.usersList = users;
                                }
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    GamePage.prototype.pushMove = function (id, move) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.moveCount++;
                        if (this.currentPlayer) {
                            this.currentPlayer.myMove = false;
                        }
                        if (!this.bot) return [3 /*break*/, 1];
                        if (!move.lastUpdateTime) {
                            move.lastUpdateTime = this.session.lastUpdateTime;
                        }
                        this.executeMoves([move]);
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].subCol(this.db, this.sessionId, move, id)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/, id];
                }
            });
        });
    };
    GamePage.prototype.pushDistributeMoveInChildSession = function (id, move) {
        id = "distribute";
        if (this.currentPlayer) {
            this.currentPlayer.myMove = false;
        }
        if (this.bot) {
            if (move.lastUpdateTime) {
                move.lastUpdateTime = this.session.lastUpdateTime;
            }
            this.executeMoves([move]);
        }
        else {
            __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].subCol(this.db, this.childSession, move, "" + new Date().getTime() + id);
        }
    };
    GamePage.prototype.closeBid = function () {
        var temp = Object.assign([], this.players);
        temp.sort(function (a, b) { return (a.bidCount < b.bidCount ? 0 : -1); });
        temp = temp
            .filter(function (a) { return a.bidCount == temp[0].bidCount; })
            .sort(function (a, b) { return (a.bidTime > b.bidTime ? 1 : -1); });
        this.bidWinner = temp[0];
        var move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
        move.command = "bidDone";
        move.data = {};
        move.data.bidWinnerIndex = this.bidWinner.index;
        move.data.mar = this.bidWinner.bidCount;
        var id = "bidDone";
        move.turn = __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */].arrageById(this.bidWinner.id, this.players)[1].index;
        this.pushMove("" + (new Date().getTime() + this.user.timeDifference) + id, move);
    };
    GamePage.prototype.getSanitizedValue = function (value) {
        var style = this.sanitizer.bypassSecurityTrustStyle(value);
        return style;
    };
    GamePage.prototype.cardClick = function (card, player) {
        var _this = this;
        console.log(this);
        if (card &&
            this.currentPlayer &&
            this.currentPlayer.id == player.id &&
            this.currentPlayer.myMove) {
            if (card.y != 0) {
                if (this.firstTurn) {
                    var move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                    var id = "settrum";
                    move.command = "settrum";
                    move.data = {};
                    move.data.card = card.name;
                    move.data.index = player.index;
                    this.pushMove("" + (new Date().getTime() + this.user.timeDifference) + id, move);
                }
                else if (this.turnCards.length == 0) {
                    var playerTurn = __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */].arrageById(player.id, this.players)[1].index;
                    this.setMove(card, player, playerTurn);
                }
                else if (this.turnCards.length == this.players.length - 1) {
                    if (this.checkIfValidMove(card, player)) {
                        var array = [];
                        for (var _i = 0, _a = this.turnCards; _i < _a.length; _i++) {
                            var turnCard_1 = _a[_i];
                            array.push(turnCard_1);
                        }
                        var turnCard = new __WEBPACK_IMPORTED_MODULE_13__models_turn__["a" /* Turn */]();
                        turnCard.card = card;
                        turnCard.player = player;
                        array.push(turnCard);
                        var containsTrum = array.filter(function (x) { return x.card.type === _this.trum; }).length > 0;
                        var series_1 = this.turnCards[0].card.type;
                        if (containsTrum) {
                            series_1 = this.trum;
                        }
                        var validCards = array.filter(function (x) { return x.card.type === series_1; });
                        var sortValues = validCards.sort(function (a, b) {
                            return a.card.points < b.card.points ? 1 : -1;
                        });
                        var winPlayer = sortValues[0].player;
                        var currentMar = this.mar;
                        if (this.bidWinner.id == winPlayer.id) {
                            currentMar++;
                        }
                        this.setMove(card, player, winPlayer.index, currentMar, true, winPlayer.index);
                    }
                }
                else {
                    if (this.checkIfValidMove(card, player)) {
                        var playerTurn = __WEBPACK_IMPORTED_MODULE_4__models_player__["a" /* Player */].arrageById(player.id, this.players)[1]
                            .index;
                        this.setMove(card, player, playerTurn);
                    }
                    else {
                        var alert_3 = this.alertCtrl.create({
                            title: "Wrong move",
                            subTitle: "Move is not valid",
                            buttons: ["Dismiss"]
                        });
                        alert_3.present();
                    }
                }
            }
            else {
                card.y = -20;
                for (var _b = 0, _c = player.playerCards; _b < _c.length; _b++) {
                    var item = _c[_b];
                    if (item.name != card.name) {
                        item.y = 0;
                    }
                }
            }
        }
    };
    GamePage.prototype.checkIfValidMove = function (card, player) {
        var currentSeries = this.turnCards[0].card.type;
        var count = player.playerCards.filter(function (x) { return x.type == currentSeries; }).length;
        if (count > 0 && card.type != currentSeries) {
            return false;
        }
        else {
            return true;
        }
    };
    GamePage.prototype.setMove = function (card, player, playerTurn, mar, complete, winnerIndex) {
        if (mar === void 0) { mar = -1; }
        if (complete === void 0) { complete = false; }
        if (winnerIndex === void 0) { winnerIndex = -1; }
        var move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
        move.command = "move";
        move.data = {};
        move.data.card = card.name;
        move.data.index = player.index;
        move.turn = playerTurn;
        move.mar = mar;
        move.winnerIndex = winnerIndex;
        move.complete = complete;
        if (winnerIndex >= 0) {
            if (this.players[0].playerCards.length === 1) {
                move.gameOver = true;
            }
        }
        var id = "move";
        this.pushMove("" + (new Date().getTime() + this.user.timeDifference) + id, move);
        this.playerClick(card, player);
    };
    GamePage.prototype.playerClick = function (card, player) {
        var index = this.players.indexOf(player);
        if (card) {
            card.opacity = 100;
            card.width = 55;
            card.height = 80;
            if (index == 0) {
                card.classes = "card cardFront plr1Card cardThrow plr1Anim";
            }
            else if (index == 1) {
                card.classes = "card cardFront plr2Card cardThrow plr2Anim";
            }
            else if (index == 2) {
                card.classes = "card cardFront plr3Card cardThrow plr3Anim";
            }
            else if (index == 3) {
                card.classes = "card cardFront plr4Card cardThrow plr4Anim";
            }
        }
    };
    GamePage.prototype.newGame = function () {
        return __awaiter(this, void 0, void 0, function () {
            var session, created, move, id;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.bot) return [3 /*break*/, 1];
                        session = this.storageService.getBotSession();
                        this.navCtrl.push(GamePage_1, {
                            gameId: this.gameId,
                            sessionId: this.sessionId,
                            session: session,
                            startGame: true,
                            bot: true
                        });
                        return [3 /*break*/, 4];
                    case 1:
                        this.session.sessionCount++;
                        this.childSession = "" + this.session.parentSession + new Date().getTime();
                        console.log(this.childSession);
                        return [4 /*yield*/, this.createSession(this.childSession)];
                    case 2:
                        created = _a.sent();
                        if (!created) return [3 /*break*/, 4];
                        // this.session.waitingPlayers.map((player) => {
                        //   this.addPlayerInNewSession(player, this.childSession, player.id)
                        // })
                        return [4 /*yield*/, this.startGameInChildSession()];
                    case 3:
                        // this.session.waitingPlayers.map((player) => {
                        //   this.addPlayerInNewSession(player, this.childSession, player.id)
                        // })
                        _a.sent();
                        move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                        id = "joinNewSession";
                        move.command = "joinNewSession";
                        move.childSessionId = this.childSession;
                        this.pushMove("" + (new Date().getTime() + this.user.timeDifference) + id, move);
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.addPlayerInNewSession = function (player, newSession, userId) {
        {
            var move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
            var id = "playerJoined";
            move.data = player;
            if (this.user.id === userId) {
                move.isHost = true;
            }
            move.command = "joined";
            __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].subCol(this.db, newSession, move, "" + new Date().getTime() + id);
        }
    };
    GamePage.prototype.createSession = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var regex, exists, s, toastOption, toastOption, ex_1, toastOption;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 7, , 8]);
                        regex = new RegExp(/^[a-zA-Z0-9]+$/);
                        if (!regex.test(data)) return [3 /*break*/, 5];
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].checkIfDocumentExists(this.db, "session", data)];
                    case 1:
                        exists = _a.sent();
                        if (!!exists) return [3 /*break*/, 3];
                        s = new __WEBPACK_IMPORTED_MODULE_8__models_session__["a" /* Session */]();
                        s.id = data;
                        s.createdBy = this.user.id;
                        s.startGame = false;
                        s.waitingPlayers = [];
                        s.sessionCount = this.session.sessionCount;
                        s.parentSession = this.session.parentSession;
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].set(this.db, "session", s, data)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, true];
                    case 3:
                        toastOption = {
                            message: "Session name already exists",
                            duration: 2000
                        };
                        this.toastCtrl.create(toastOption).present();
                        window.location.href = this.getChildSessionUrl(this.childSession);
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        toastOption = {
                            message: "Only letters and numbers allowed in session name",
                            duration: 5000
                        };
                        this.toastCtrl.create(toastOption).present();
                        return [2 /*return*/, false];
                    case 6: return [3 /*break*/, 8];
                    case 7:
                        ex_1 = _a.sent();
                        toastOption = {
                            message: "Something went wrong please try again later",
                            duration: 2000
                        };
                        return [3 /*break*/, 8];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.setSession = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var sessionSet, _a, toastOption;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.storageService.setSession(name)];
                    case 1:
                        sessionSet = _b.sent();
                        if (!sessionSet) return [3 /*break*/, 3];
                        _a = this;
                        return [4 /*yield*/, this.storageService.getSession()];
                    case 2:
                        _a.session = _b.sent();
                        this.sessionId = name;
                        this.setPlayers();
                        return [3 /*break*/, 4];
                    case 3:
                        toastOption = {
                            message: "Unable to join sesson",
                            duration: 3000
                        };
                        this.toastCtrl.create(toastOption).present();
                        _b.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.updateSession = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].set(this.db, "session", this.session, this.sessionId)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.sendMessage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.messageText) return [3 /*break*/, 2];
                        message = new __WEBPACK_IMPORTED_MODULE_16__models_message__["a" /* Message */]();
                        message.type = 0;
                        message.text = this.messageText;
                        this.messageText = "";
                        return [4 /*yield*/, this.pushMessage(message)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.pushMessage = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var id, sent, items, index;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        id = new Date().getTime();
                        message.name = this.user.name;
                        message.userId = this.user.id;
                        message.id = id;
                        this.messages.push(message);
                        this.scrollToBottom();
                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].add(this.db, "session_" + this.sessionId, message)];
                    case 1:
                        sent = _a.sent();
                        if (!sent) {
                            items = this.messages.filter(function (x) { return x.id == id; });
                            if (items.length > 0) {
                                index = this.messages.indexOf(items[0]);
                                this.messages.splice(index, 1);
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    GamePage.prototype.scrollToBottom = function () {
        var _this = this;
        setTimeout(function () {
            try {
                _this.myScrollContainer.nativeElement.scrollTop = _this.myScrollContainer.nativeElement.scrollHeight;
            }
            catch (err) { }
        }, 500);
    };
    GamePage.prototype.maintainWaitingPlayers = function () {
        var _this = this;
        if (!this.bot) {
            this.usersList.map(function (value, index) {
                if (!value.online) {
                    var offlineCounter = Math.round(((new Date().getTime() + _this.user.timeDifference) - value.lastUpdateTime) / 1000);
                    if (offlineCounter > 30) {
                        _this.session.waitingPlayers.map(function (value2, index2) { return __awaiter(_this, void 0, void 0, function () {
                            var move, id, document_2;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!(value.id === value2.id)) return [3 /*break*/, 3];
                                        this.session.waitingPlayers.splice(index2, 1);
                                        this.removeUser(value.id);
                                        move = new __WEBPACK_IMPORTED_MODULE_10__models_move__["a" /* Move */]();
                                        move.command = "disconnect";
                                        move.disconnectId = value.id;
                                        id = "playerDisconnect";
                                        return [4 /*yield*/, this.pushMove("" + (new Date().getTime() + this.user.timeDifference) + id, move)];
                                    case 1:
                                        //change
                                        id = _a.sent();
                                        return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_9__helper_fireStoreHelper__["a" /* FireStoreHelper */].getSubCollectionDocument(this.db, "session", this.session.id, "moves", id)];
                                    case 2:
                                        document_2 = _a.sent();
                                        move = Object.assign(move, document_2.data());
                                        this.executeMoves([move]);
                                        _a.label = 3;
                                    case 3: return [2 /*return*/];
                                }
                            });
                        }); });
                    }
                }
            });
        }
    };
    var GamePage_1;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])("game"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], GamePage.prototype, "game", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])("chatScroll"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], GamePage.prototype, "myScrollContainer", void 0);
    GamePage = GamePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: "page-game",template:/*ion-inline-start:"C:\Users\one\Desktop\projects\martor\src\pages\game\game.html"*/'<ion-content no-bounce>\n  <!-- <ion-grid #game>\n    <ion-row style="height:34px">\n      <ion-col color="secondary" class="score" col-5 text-center>\n        <span style="vertical-align:top;" *ngIf="!this.gameOver">\n            <span style="vertical-align:top;" *ngIf="!this.gameOver">\n              <span style="font-weight:bold;font-size:16px;" *ngIf="this.user">Coins:<span style="font-size:20px;font-weight:bold;">{{this.user.points}}</span></span>\n            </span>\n            <span style="font-weight:bold;font-size:16px;" *ngIf="biddingDone">{{marName}}:<span style="font-size:20px;font-weight:bold;">{{mar}}/{{marCount}}</span></span>\n            <span>{{lastResult}}</span>\n        </span>\n      </ion-col>\n      <ion-col text-center *ngIf="players[2]">\n        <img *ngFor="let card of players[2].playerCards" [id]="card.name" [style.transform]="getSanitizedValue(card.transform)"\n          [style.opacity]="card.opacity" class="othercard" [style.width.px]="card.width" [style.height.px]="card.height"\n          [src]="card.imageUrl">\n        <span class="playerBox" style="vertical-align:top;">\n          {{players[2].name}}</span>\n        <br /><span class="timer" *ngIf="currentPlayer && currentPlayer.id==players[2].id">{{currentPlayer.timeElapse}}\n        </span>\n        <img *ngIf="getBidResult(players[2])" style="width:16px;height:16px;" src="assets/imgs/biddingdone.png" alt="Bidding Done" />\n        <img *ngIf="!biddingDone && players[2].bidCount<0" style="width:16px;height:16px;" src="assets/imgs/biddingpending.png"\n          alt="Bidding Pending" />\n      </ion-col>\n      <ion-col color="primary" class="score" col-5 text-center>\n        <span *ngIf="trum" style="font-size:24px;">{{getTrumSymbol()}}</span>\n        <span *ngIf="!gameOver">\n          <button ion-button small color="primary" *ngIf="showBidButton && startGame && partOfGame && !biddingDone && !isBiddingComplete" (click)="openBid()">Bid</button>\n          <button ion-button small color="primary" *ngIf="bot && !biddingDone && isHost() && isBiddingComplete" (click)="closeBid()">Close\n            Bid</button>\n          <button ion-button small color="primary" *ngIf="canSetTrum()" (click)="selectTrum()">Set Trum</button>\n        </span>\n\n        <ion-icon name="menu" style="float:right;padding-right:4px;font-size:30px;" (click)="showactionsheet()"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row style="height:70%;">\n      <ion-col col-4 *ngIf="players[1]" col-3 align-self-center text-left>\n        <img *ngFor="let card of players[1].playerCards" [id]="card.name" [style.transform]="getSanitizedValue(card.transform)"\n          [style.opacity]="card.opacity" [style.width.px]="card.width" [style.height.px]="card.height" class="othercard"\n          [src]="card.imageUrl">\n        <span class="playerBox">\n          {{players[1].name}}\n        </span>\n        <span class="timer" *ngIf="currentPlayer && currentPlayer.id==players[1].id">{{currentPlayer.timeElapse}}</span>\n\n        <img *ngIf="getBidResult(players[1])" style="width:16px;height:16px;" src="assets/imgs/biddingdone.png" alt="Bidding Done" />\n        <img *ngIf="partOfGame && !biddingDone && players[1].bidCount<0" style="width:16px;height:16px;" src="assets/imgs/biddingpending.png"\n          alt="Bidding Pending" />\n      </ion-col>\n      <ion-col col-4 align-self-center text-center>\n          <span class="timer" *ngIf="!gameOver && partOfGame && currentPlayer && currentPlayer.id==players[0].id">{{currentPlayer.timeElapse}}</span>\n\n      </ion-col>\n      <ion-col col-4 *ngIf="players[3]" col-3 align-self-center text-right>\n        <img *ngFor="let card of players[3].playerCards" [id]="card.name" [style.transform]="getSanitizedValue(card.transform)"\n          [style.opacity]="card.opacity" [style.width.px]="card.width" [style.height.px]="card.height" class="othercard"\n          [src]="card.imageUrl">\n        <img *ngIf="getBidResult(players[3])" style="width:16px;height:16px;" src="assets/imgs/biddingdone.png" alt="Bidding Done" />\n        <img *ngIf="!biddingDone && players[3].bidCount<0" style="width:16px;height:16px;" src="assets/imgs/biddingpending.png"\n          alt="Bidding Pending" />\n        <span class="timer" *ngIf="currentPlayer && currentPlayer.id==players[3].id">{{currentPlayer.timeElapse}}</span>\n        <span class="playerBox">\n          {{players[3].name}}\n        </span>\n\n      </ion-col>\n\n    </ion-row>\n    <ion-row style="height:80px" *ngIf="!partOfGame">\n      <ion-col *ngIf="players[0]" align-self-center text-center col-12>\n        <span *ngFor="let card of players[0].playerCards">\n          <img *ngIf="canSeeCards(card)" (click)="cardClick(card,players[0])" [id]="card.name" [style.transform]="getSanitizedValue(card.transform)"\n            [style.width.px]="card.width" [style.height.px]="card.height" class="card" [src]="card.imageUrl">\n        </span>\n        <span class="timer" *ngIf="currentPlayer && currentPlayer.id==players[0].id">{{currentPlayer.timeElapse}}</span>\n        <br />\n        <span class="playerBox">\n          {{players[0].name}}\n        </span>\n        <img *ngIf="getBidResult(players[0])" style="width:16px;height:16px;" src="assets/imgs/biddingdone.png" alt="Bidding Done" />\n        <img *ngIf="!biddingDone && players[0].bidCount<0" style="width:16px;height:16px;" src="assets/imgs/biddingpending.png"\n          alt="Bidding Pending" />\n\n\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <div class="chatBox" *ngIf="!bot" [style.width]="chatBoxWidth" [style.height]="chatBoxHeight"  >\n      <div style="overflow:hidden;width:100%;height:100%;">\n        <div #chatScroll style="overflow-y:scroll;width:100%;height:100%;">\n          <div *ngFor="let chat of messages| sort:\'id\'" no-lines>\n            <div class="chat-message" text-right *ngIf="chat.userId === user.id">\n              <div class="right-bubble">\n                <span class="msg-name">Me</span>\n                <span class="msg-date">{{chat.time}}</span>\n                <div text-wrap *ngIf="chat.type==0">{{chat.text}}</div>\n                <div text-wrap *ngIf="chat.type==1">\n                  <audio controls>\n                    <source [src]="chat.text" type="audio/webm">\n                    Your browser does not support the audio element.\n                  </audio></div>\n              </div>\n            </div>\n            <div class="chat-message" text-left *ngIf="chat.userId !== user.id">\n              <div class="left-bubble">\n                <span class="msg-name">{{chat.name}}</span>\n                <span class="msg-date">{{chat.time}}</span>\n                <div text-wrap *ngIf="chat.type==0">{{chat.text}}</div>\n                <div text-wrap *ngIf="chat.type==1">\n                  <audio controls autoplay>\n                  <source [src]="chat.text" type="audio/webm">\n                  Your browser does not support the audio element.\n                </audio>\n              </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <span *ngIf="partOfGame && players[0]" style="position:absolute;left:0;" [style.top]="cardsTop" >\n        <span *ngFor="let card of players[0].playerCards">\n          <img *ngIf="canSeeCards(card)" (click)="cardClick(card,players[0])" [id]="card.name" [style.transform]="getSanitizedValue(card.transform)"\n            [style.width.px]="card.width" [style.height.px]="card.height" class="card" [src]="card.imageUrl">\n        </span>\n      </span> -->\n\n  <section #game class="gameScreen">\n    <div class="container-fluid">\n\n      <div class="menuOption" *ngIf="!this.gameOver">\n          <ion-icon name="menu" class="menuBtn"  (click)="showactionsheet()"></ion-icon>\n        <div class="playerScore" *ngIf="!this.gameOver">\n          💰{{this.user.points}}\n        </div>\n        <span  *ngIf="biddingDone" class="bidRst">{{marName}}:<span >{{mar}}/{{marCount}}</span></span>\n        <span>{{lastResult}}</span>\n        <span *ngIf="trum" class="trum">{{getTrumSymbol()}}</span>\n      </div>\n      <div class="gameOption">\n        <a href="#" class="btn" id="gameButton">🔊</a>\n        <a href="#" class="btn" id="gameButton">🔔</a>\n        <a href="#" class="btn" id="gameButton">👥</a>\n        <button type="button" class="btn btn-lg" data-toggle="modal" data-target="#messages" id="gameButton">💬</button>\n      </div>\n      <div class="game">\n        <div class="player1" *ngIf="players[2]">\n            <div  *ngFor="let card of players[2].playerCards" [ngClass]="card.classes ?card.classes:\'card cardFront plr3Card\'">\n                <div  [style.color]="card.type==\'H\' || card.type==\'D\'?\'#f00\':\'black\'">\n                  <div  [ngClass]="card.classes? \'throwTopNum\':\'topNum\'"><span [ngClass]="card.classes? \'throwCardNumber\':\'cardNumber\'">{{card.symbol}}</span><span [ngClass]="card.classes? \'throwRungIcon\':\'rungIcon\'">{{card.rung}}</span></div><span\n                  [ngClass]="card.classes? \'throwRung\':\'rung\'">{{card.rung}}</span>\n                </div>\n              </div>\n          <div class="details">\n          <img src="assets/imgs/p1.jpeg" class="profileImage">\n          <h5 class="profileName">{{players[2].name}}</h5>\n        </div>\n        </div>\n        <div class="player2" *ngIf="players[1]">\n          <div  *ngFor="let card of players[1].playerCards" [ngClass]="card.classes ?card.classes:\'card cardFront plr2Card\'">\n            <div  [style.color]="card.type==\'H\' || card.type==\'D\'?\'#f00\':\'black\'">\n              <div  [ngClass]="card.classes? \'throwTopNum\':\'topNum\'"><span [ngClass]="card.classes? \'throwCardNumber\':\'cardNumber\'">{{card.symbol}}</span><span [ngClass]="card.classes? \'throwRungIcon\':\'rungIcon\'">{{card.rung}}</span></div><span\n              [ngClass]="card.classes? \'throwRung\':\'rung\'">{{card.rung}}</span>\n            </div>\n          </div>\n          <div class="details">\n            <img src="assets/imgs/p1.jpeg" class="profileImage">\n            <h5 class="profileName">{{players[1].name}}</h5>\n          </div>\n          <!-- <div class="card2">\n              <div class="cardThrow">\n                <div class="throwTopNum"><span class="throwCardNumber">A</span><span class="throwRungIcon">♦</span></div><span\n                  class="throwRung">♦</span>\n              </div>\n      </div> -->\n        </div>\n        <span class="bidBtn" *ngIf="!gameOver">\n          <button ion-button small color="primary" *ngIf="showBidButton && startGame && partOfGame && !biddingDone && !isBiddingComplete" (click)="openBid()">Bid</button>\n          <button ion-button small color="primary" *ngIf="bot && !biddingDone && isHost() && isBiddingComplete" (click)="closeBid()">Close\n            Bid</button>\n          <button ion-button small color="primary" *ngIf="canSetTrum()" (click)="selectTrum()">Set Trum</button>\n        </span>\n\n        <div class="currentPlayer d-flex" *ngIf="players[0]">\n            <div class="details" >\n                <img src="assets/imgs/p1.jpeg" class="profileImage">\n                <h5 class="profileName">{{players[0].name}}</h5>\n              </div>\n\n          <div id="baseRow" *ngIf="partOfGame && players[0]" class="cards d-flex">\n            <button  *ngFor="let card of players[0].playerCards" (click)="cardClick(card,players[0])" class="card" [ngClass]="card.classes ?card.classes:\'card cardFront\'">\n              <div [ngClass]="card.classes? \'cardThrow\':\'cardFront\'" [style.color]="card.type==\'H\' || card.type==\'D\'?\'#f00\':\'black\'">\n                <div [ngClass]="card.classes? \'throwTopNum\':\'topNum\'"><span [ngClass]="card.classes? \'throwCardNumber\':\'cardNumber\'">{{card.symbol}}</span><span [ngClass]="card.classes? \'throwRungIcon\':\'rungIcon\'">{{card.rung}}</span></div><span\n                [ngClass]="card.classes? \'throwRung\':\'rung\'">{{card.rung}}</span>\n              </div>\n            </button>\n          </div>\n        </div>\n        <div class="player4" *ngIf="players[3]">\n            <div  *ngFor="let card of players[3].playerCards" [ngClass]="card.classes ?card.classes:\'card cardFront plr4Card\'">\n            <div  [style.color]="card.type==\'H\' || card.type==\'D\'?\'#f00\':\'black\'">\n              <div  [ngClass]="card.classes? \'throwTopNum\':\'topNum\'"><span [ngClass]="card.classes? \'throwCardNumber\':\'cardNumber\'">{{card.symbol}}</span><span [ngClass]="card.classes? \'throwRungIcon\':\'rungIcon\'">{{card.rung}}</span></div><span\n              [ngClass]="card.classes? \'throwRung\':\'rung\'">{{card.rung}}</span>\n            </div>\n          </div>\n          <div class="details">\n            <img src="assets/imgs/p1.jpeg" class="profileImage">\n            <h5 class="profileName">{{players[3].name}}</h5>\n          </div>\n\n        </div>\n      </div>\n    </div>\n\n  </section>\n\n\n\n</ion-content>\n\n\n\n<!-- <ion-footer *ngIf="!bot" keyboard-attach class="bar-stable item-input-inset">\n  <ion-grid>\n    <ion-row>\n      <ion-col col-8>\n        <ion-input type="text" [(ngModel)]="messageText" (keyup.enter)="sendMessage()"  placeholder="Type a message" name="message"></ion-input>\n      </ion-col>\n      <ion-col col-4 (click)="sendMessage()">\n        <ion-icon name="paper-plane"></ion-icon>\n        <voicenote (onFileUpload)="onFileUpload($event)"></voicenote>\n\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer> -->\n'/*ion-inline-end:"C:\Users\one\Desktop\projects\martor\src\pages\game\game.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__node_modules_angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["AngularFirestore"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_7__services_storageservice__["a" /* StorageService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */]])
    ], GamePage);
    return GamePage;
}());

//tbc x.card is undefined
//firebase function
// const functions = require('firebase-functions');
// const Firestore = require('@google-cloud/firestore');
// const firebase = require('firebase');
// const firestore = new Firestore();
// exports.onUserStatusChanged = functions.database
//   .ref('/status/{userId}')
//   .onUpdate((change, context) => {
//     const usersRef = firestore.collection('/users');
//     return change.after.ref.once('value')
//       .then(statusSnapshot => statusSnapshot.val())
//       .then(status => {
//         if (status === 'offline') {
//           let path = change.after._path.substring(8);
//           usersRef
//             .doc(path)
//             .set({
//               online: false,
//               lastUpdateTime : Date.now()
//             }, { merge: true });
//         }
//         return true;
//       })
//   });
//# sourceMappingURL=game.js.map

/***/ }),

/***/ 422:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Card; });
var Card = /** @class */ (function () {
    function Card() {
        this.rotation = 0;
        this.x = 0;
        this.y = 0;
        this.opacity = 100;
        this.used = false;
        this.width = 70;
        this.height = 110;
        this.clicked = false;
        this.rank = 0;
    }
    Object.defineProperty(Card.prototype, "transform", {
        get: function () {
            return 'translate(' + this.x + 'px,' + this.y + 'px) rotate(' + this.rotation + 'deg)';
        },
        enumerable: true,
        configurable: true
    });
    Card.GenerateSeries = function (type) {
        var cards = [];
        for (var i = 2; i <= 14; i++) {
            cards.push(Card.getCard(type, i));
        }
        return cards;
    };
    Card.getSeriesType = function () {
        return ["C", "D", "S", "D"];
    };
    Card.getCardByName = function (name) {
        var type = name.substring(name.length - 1, name.length);
        var points = parseInt(name.substring(0, name.length - 1));
        return Card.getCard(type, points);
    };
    Card.getCard = function (type, points) {
        var card = new Card();
        card.points = points;
        card.type = type;
        card.name = points + type;
        card.imageUrl = 'assets/imgs/cards/' + Card.getName(points, type) + '.png';
        return card;
    };
    Card.deck = function () {
        var cards = [];
        cards = cards.concat(Card.GenerateSeries("C"));
        cards = cards.concat(Card.GenerateSeries("D"));
        cards = cards.concat(Card.GenerateSeries("H"));
        cards = cards.concat(Card.GenerateSeries("S"));
        return cards;
        ;
    };
    Card.getName = function (i, type) {
        if (i == 11) {
            return "J" + type;
        }
        else if (i == 12) {
            return "Q" + type;
        }
        else if (i == 13) {
            return "K" + type;
        }
        else if (i == 14) {
            return "A" + type;
        }
        else {
            return i + type;
        }
    };
    return Card;
}());

//# sourceMappingURL=card.js.map

/***/ }),

/***/ 423:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Move; });
var Move = /** @class */ (function () {
    function Move() {
        this.complete = false;
        this.gameOver = false;
    }
    return Move;
}());

//# sourceMappingURL=move.js.map

/***/ }),

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(559);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 559:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2__ = __webpack_require__(607);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_angularfire2__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_firestore__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angularfire2_firestore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__credentials__ = __webpack_require__(619);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(620);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_game_game__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_storageservice__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_callingService__ = __webpack_require__(636);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pipe_arraySortPipe__ = __webpack_require__(637);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_ionic_long_press__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_components_module__ = __webpack_require__(638);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_common_http__ = __webpack_require__(425);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_game_game__["a" /* GamePage */],
                __WEBPACK_IMPORTED_MODULE_13__pipe_arraySortPipe__["a" /* ArraySortPipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_14_ionic_long_press__["a" /* LongPressModule */],
                __WEBPACK_IMPORTED_MODULE_5_angularfire2__["AngularFireModule"].initializeApp(__WEBPACK_IMPORTED_MODULE_7__credentials__["a" /* firebaseConfig */]),
                __WEBPACK_IMPORTED_MODULE_6_angularfire2_firestore__["AngularFirestoreModule"],
                __WEBPACK_IMPORTED_MODULE_15__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_16__angular_common_http__["b" /* HttpClientModule */]
            ],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_game_game__["a" /* GamePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_11__services_storageservice__["a" /* StorageService */],
                __WEBPACK_IMPORTED_MODULE_12__services_callingService__["a" /* CallingService */],
                __WEBPACK_IMPORTED_MODULE_6_angularfire2_firestore__["AngularFirestore"],
                __WEBPACK_IMPORTED_MODULE_16__angular_common_http__["b" /* HttpClientModule */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 619:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return firebaseConfig; });
var firebaseConfig = {
    apiKey: "AIzaSyC6wNGeSM2Z4YD60BnNYc2-bfP9955aiPU",
    authDomain: "martor-4c241.firebaseapp.com",
    databaseURL: "https://martor-4c241.firebaseio.com",
    projectId: "martor-4c241",
    storageBucket: "martor-4c241.appspot.com",
    messagingSenderId: "765287473026"
};
//# sourceMappingURL=credentials.js.map

/***/ }),

/***/ 620:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(289);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_storageservice__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, storageService) {
        this.storageService = storageService;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp.prototype.ngOnInit = function () {
        this.storageService.getUser(true);
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"C:\Users\one\Desktop\projects\martor\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\one\Desktop\projects\martor\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_5__services_storageservice__["a" /* StorageService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 626:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 297,
	"./af.js": 297,
	"./ar": 298,
	"./ar-dz": 299,
	"./ar-dz.js": 299,
	"./ar-kw": 300,
	"./ar-kw.js": 300,
	"./ar-ly": 301,
	"./ar-ly.js": 301,
	"./ar-ma": 302,
	"./ar-ma.js": 302,
	"./ar-sa": 303,
	"./ar-sa.js": 303,
	"./ar-tn": 304,
	"./ar-tn.js": 304,
	"./ar.js": 298,
	"./az": 305,
	"./az.js": 305,
	"./be": 306,
	"./be.js": 306,
	"./bg": 307,
	"./bg.js": 307,
	"./bm": 308,
	"./bm.js": 308,
	"./bn": 309,
	"./bn.js": 309,
	"./bo": 310,
	"./bo.js": 310,
	"./br": 311,
	"./br.js": 311,
	"./bs": 312,
	"./bs.js": 312,
	"./ca": 313,
	"./ca.js": 313,
	"./cs": 314,
	"./cs.js": 314,
	"./cv": 315,
	"./cv.js": 315,
	"./cy": 316,
	"./cy.js": 316,
	"./da": 317,
	"./da.js": 317,
	"./de": 318,
	"./de-at": 319,
	"./de-at.js": 319,
	"./de-ch": 320,
	"./de-ch.js": 320,
	"./de.js": 318,
	"./dv": 321,
	"./dv.js": 321,
	"./el": 322,
	"./el.js": 322,
	"./en-au": 323,
	"./en-au.js": 323,
	"./en-ca": 324,
	"./en-ca.js": 324,
	"./en-gb": 325,
	"./en-gb.js": 325,
	"./en-ie": 326,
	"./en-ie.js": 326,
	"./en-il": 327,
	"./en-il.js": 327,
	"./en-nz": 328,
	"./en-nz.js": 328,
	"./eo": 329,
	"./eo.js": 329,
	"./es": 330,
	"./es-do": 331,
	"./es-do.js": 331,
	"./es-us": 332,
	"./es-us.js": 332,
	"./es.js": 330,
	"./et": 333,
	"./et.js": 333,
	"./eu": 334,
	"./eu.js": 334,
	"./fa": 335,
	"./fa.js": 335,
	"./fi": 336,
	"./fi.js": 336,
	"./fo": 337,
	"./fo.js": 337,
	"./fr": 338,
	"./fr-ca": 339,
	"./fr-ca.js": 339,
	"./fr-ch": 340,
	"./fr-ch.js": 340,
	"./fr.js": 338,
	"./fy": 341,
	"./fy.js": 341,
	"./gd": 342,
	"./gd.js": 342,
	"./gl": 343,
	"./gl.js": 343,
	"./gom-latn": 344,
	"./gom-latn.js": 344,
	"./gu": 345,
	"./gu.js": 345,
	"./he": 346,
	"./he.js": 346,
	"./hi": 347,
	"./hi.js": 347,
	"./hr": 348,
	"./hr.js": 348,
	"./hu": 349,
	"./hu.js": 349,
	"./hy-am": 350,
	"./hy-am.js": 350,
	"./id": 351,
	"./id.js": 351,
	"./is": 352,
	"./is.js": 352,
	"./it": 353,
	"./it.js": 353,
	"./ja": 354,
	"./ja.js": 354,
	"./jv": 355,
	"./jv.js": 355,
	"./ka": 356,
	"./ka.js": 356,
	"./kk": 357,
	"./kk.js": 357,
	"./km": 358,
	"./km.js": 358,
	"./kn": 359,
	"./kn.js": 359,
	"./ko": 360,
	"./ko.js": 360,
	"./ku": 361,
	"./ku.js": 361,
	"./ky": 362,
	"./ky.js": 362,
	"./lb": 363,
	"./lb.js": 363,
	"./lo": 364,
	"./lo.js": 364,
	"./lt": 365,
	"./lt.js": 365,
	"./lv": 366,
	"./lv.js": 366,
	"./me": 367,
	"./me.js": 367,
	"./mi": 368,
	"./mi.js": 368,
	"./mk": 369,
	"./mk.js": 369,
	"./ml": 370,
	"./ml.js": 370,
	"./mn": 371,
	"./mn.js": 371,
	"./mr": 372,
	"./mr.js": 372,
	"./ms": 373,
	"./ms-my": 374,
	"./ms-my.js": 374,
	"./ms.js": 373,
	"./mt": 375,
	"./mt.js": 375,
	"./my": 376,
	"./my.js": 376,
	"./nb": 377,
	"./nb.js": 377,
	"./ne": 378,
	"./ne.js": 378,
	"./nl": 379,
	"./nl-be": 380,
	"./nl-be.js": 380,
	"./nl.js": 379,
	"./nn": 381,
	"./nn.js": 381,
	"./pa-in": 382,
	"./pa-in.js": 382,
	"./pl": 383,
	"./pl.js": 383,
	"./pt": 384,
	"./pt-br": 385,
	"./pt-br.js": 385,
	"./pt.js": 384,
	"./ro": 386,
	"./ro.js": 386,
	"./ru": 387,
	"./ru.js": 387,
	"./sd": 388,
	"./sd.js": 388,
	"./se": 389,
	"./se.js": 389,
	"./si": 390,
	"./si.js": 390,
	"./sk": 391,
	"./sk.js": 391,
	"./sl": 392,
	"./sl.js": 392,
	"./sq": 393,
	"./sq.js": 393,
	"./sr": 394,
	"./sr-cyrl": 395,
	"./sr-cyrl.js": 395,
	"./sr.js": 394,
	"./ss": 396,
	"./ss.js": 396,
	"./sv": 397,
	"./sv.js": 397,
	"./sw": 398,
	"./sw.js": 398,
	"./ta": 399,
	"./ta.js": 399,
	"./te": 400,
	"./te.js": 400,
	"./tet": 401,
	"./tet.js": 401,
	"./tg": 402,
	"./tg.js": 402,
	"./th": 403,
	"./th.js": 403,
	"./tl-ph": 404,
	"./tl-ph.js": 404,
	"./tlh": 405,
	"./tlh.js": 405,
	"./tr": 406,
	"./tr.js": 406,
	"./tzl": 407,
	"./tzl.js": 407,
	"./tzm": 408,
	"./tzm-latn": 409,
	"./tzm-latn.js": 409,
	"./tzm.js": 408,
	"./ug-cn": 410,
	"./ug-cn.js": 410,
	"./uk": 411,
	"./uk.js": 411,
	"./ur": 412,
	"./ur.js": 412,
	"./uz": 413,
	"./uz-latn": 414,
	"./uz-latn.js": 414,
	"./uz.js": 413,
	"./vi": 415,
	"./vi.js": 415,
	"./x-pseudo": 416,
	"./x-pseudo.js": 416,
	"./yo": 417,
	"./yo.js": 417,
	"./zh-cn": 418,
	"./zh-cn.js": 418,
	"./zh-hk": 419,
	"./zh-hk.js": 419,
	"./zh-tw": 420,
	"./zh-tw.js": 420
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 626;

/***/ }),

/***/ 627:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Deck; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__card__ = __webpack_require__(422);

var Deck = /** @class */ (function () {
    function Deck(cards, players, canvasWidth, canvasHeight) {
        this.cards = cards;
        this.players = players;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        if (players.length == 3) {
            this.cards.splice(0, 1);
        }
    }
    Deck.prototype.shuffleCards = function () {
        var index = Math.floor(Math.random() * 30);
        var cardsShuffled = this.cards.splice(index, 20);
        for (var i = 0; i < cardsShuffled.length; i++) {
            this.cards.unshift(cardsShuffled[i]);
        }
    };
    Deck.prototype.distributeCard = function (count) {
        for (var _i = 0, _a = this.players; _i < _a.length; _i++) {
            var player = _a[_i];
            var temp = this.cards.splice(0, count);
            var array = [];
            if (!player.cards) {
                player.cards = [];
            }
            for (var _b = 0, temp_1 = temp; _b < temp_1.length; _b++) {
                var card = temp_1[_b];
                array.push(card.name);
            }
            player.cards = player.cards.concat(array);
        }
    };
    Deck.prototype.distributeCards = function () {
        this.distributeCard(5);
        this.distributeCard(4);
        this.distributeCard(4);
        if (this.players.length == 3) {
            this.distributeCard(4);
        }
    };
    Deck.playOtherCard = function (players, turns, currnetPlayer, bidWinner, trum) {
        Deck.rankCards(players, currnetPlayer, bidWinner, trum);
        var turnBW = null;
        if (turns.filter(function (x) { return x.player.id == bidWinner.id; }).length > 0) {
            turnBW = turns.filter(function (x) { return x.player.id == bidWinner.id; })[0];
        }
        var type = turns[0].card.type;
        if (turns.filter(function (x) { return x.card.type == trum; }).length > 0) {
            type = trum;
        }
        var validCards = turns.filter(function (x) { return x.card.type == type; });
        var winTurn = validCards.sort(function (a, b) { return a.card.points < b.card.points ? 1 : -1; })[0];
        if (turnBW) {
            if (winTurn.player.id == bidWinner.id) {
                var array = currnetPlayer.playerCards.filter(function (x) { return x.type == winTurn.card.type && x.points > winTurn.card.points; });
                if (array.length > 0) {
                    if (winTurn.card.type != turns[0].card.type) {
                        var items = currnetPlayer.playerCards.filter(function (x) { return x.type == turns[0].card.type; });
                        if (items.length > 0) {
                            return items[0];
                        }
                        else {
                            return array[0];
                        }
                    }
                    else {
                        return array[0];
                    }
                }
                else {
                    array = currnetPlayer.playerCards.filter(function (x) { return x.type == turns[0].card.type; });
                    if (array.length > 0) {
                        return array[0];
                    }
                    else {
                        array = currnetPlayer.playerCards.filter(function (x) { return x.type == trum; });
                        if (array.length > 0) {
                            return array[0];
                        }
                        else {
                            return Deck.getLeastCard(currnetPlayer);
                        }
                    }
                }
            }
            else {
                var array = currnetPlayer.playerCards.filter(function (x) { return x.type == turns[0].card.type; });
                if (array.length > 0) {
                    return array[0];
                }
                else {
                    return Deck.getLeastCard(currnetPlayer);
                }
            }
        }
        else {
            if (currnetPlayer.playerCards.filter(function (x) { return x.type == turns[0].card.type; }).length > 0) {
                var array = currnetPlayer.playerCards.filter(function (x) { return x.type == turns[0].card.type && x.points > winTurn.card.points; });
                var card = array[array.length - 1];
                if (array.length > 0 && (card.points - winTurn.card.points) > 1) {
                    return card;
                }
                else {
                    array = currnetPlayer.playerCards.filter(function (x) { return x.type == turns[0].card.type; });
                    if (array.length > 0) {
                        return array[0];
                    }
                    else {
                        return Deck.getLeastCard(currnetPlayer);
                    }
                }
            }
            else {
                return Deck.getLeastCard(currnetPlayer);
            }
        }
    };
    Deck.getLeastCard = function (currnetPlayer) {
        var array = currnetPlayer.playerCards.filter(function (x) { return x.rank == -1; });
        if (array.length > 0) {
            return array[0];
        }
        else {
            return currnetPlayer.playerCards.sort(function (a, b) { return a.rank < b.rank ? 1 : -1; })[0];
        }
    };
    Deck.rankCards = function (players, currentPlayer, bidWinner, trum) {
        var cards = [];
        for (var _i = 0, players_1 = players; _i < players_1.length; _i++) {
            var player = players_1[_i];
            if (player.id != currentPlayer.id) {
                cards = cards.concat(player.playerCards);
            }
        }
        var _loop_1 = function (card) {
            if (card.type == trum) {
                card.rank = 0;
            }
            else if (bidWinner.seriesUsed.length == 0 || bidWinner.seriesUsed.indexOf(card.type) != -1) {
                card.rank = cards.filter(function (x) { return x.type == card.type && x.points > card.points; }).length;
            }
            else {
                card.rank = -1;
            }
        };
        for (var _a = 0, _b = currentPlayer.playerCards; _a < _b.length; _a++) {
            var card = _b[_a];
            _loop_1(card);
        }
    };
    Deck.playFirstCard = function (players, currentPlayer, bidWinner, trum) {
        Deck.rankCards(players, currentPlayer, bidWinner, trum);
        var array = [];
        var _loop_2 = function (type) {
            if (type != trum) {
                array = currentPlayer.playerCards.filter(function (x) { return x.type == type && x.rank == 0; });
                if (array.length > 1) {
                    return { value: array[0] };
                }
            }
        };
        for (var _i = 0, _a = __WEBPACK_IMPORTED_MODULE_0__card__["a" /* Card */].getSeriesType(); _i < _a.length; _i++) {
            var type = _a[_i];
            var state_1 = _loop_2(type);
            if (typeof state_1 === "object")
                return state_1.value;
        }
        array = currentPlayer.playerCards.filter(function (x) { return x.rank == -1 && x.type != trum; });
        if (array.length > 0) {
            return Deck.getRandomCard(array);
        }
        var _loop_3 = function (i) {
            array = currentPlayer.playerCards.filter(function (x) { return x.rank == i && x.type != trum; });
            if (array.length > 0) {
                return { value: Deck.getRandomCard(array) };
            }
        };
        for (var i = 1; i <= 12; i++) {
            var state_2 = _loop_3(i);
            if (typeof state_2 === "object")
                return state_2.value;
        }
        array = currentPlayer.playerCards.filter(function (x) { return x.rank == 0 && x.type != trum; });
        if (array.length > 0) {
            return Deck.getRandomCard(array);
        }
        array = currentPlayer.playerCards.filter(function (x) { return x.type != trum; });
        if (array.length > 0) {
            return array[array.length - 1];
        }
        return Deck.getRandomCard(currentPlayer.playerCards);
    };
    Deck.getRandomCard = function (array) {
        var index = Math.floor(Math.random() * array.length);
        return array[index];
    };
    Deck.arrangeCards = function (player, width, isFirstPlayer, degree, opacity) {
        var cards = [];
        var spades = [];
        var diamonds = [];
        var hearts = [];
        var clubs = [];
        for (var _i = 0, _a = player.cards; _i < _a.length; _i++) {
            var name_1 = _a[_i];
            var item = __WEBPACK_IMPORTED_MODULE_0__card__["a" /* Card */].getCardByName(name_1);
            var card = new __WEBPACK_IMPORTED_MODULE_0__card__["a" /* Card */]();
            card.type = item.type;
            card.name = item.name;
            card.opacity = item.opacity;
            card.points = item.points;
            if (card.points > 10) {
                if (card.points == 11) {
                    card.symbol = "J";
                }
                else if (card.points == 12) {
                    card.symbol = "Q";
                }
                else if (card.points == 13) {
                    card.symbol = "K";
                }
                else if (card.points == 14) {
                    card.symbol = "A";
                }
            }
            else {
                card.symbol = card.points.toString();
            }
            card.used = item.used;
            card.imageUrl = item.imageUrl;
            if (card.type == "S") {
                card.rung = "♠";
                spades.push(card);
            }
            else if (card.type == "H") {
                card.rung = "♥";
                hearts.push(card);
            }
            else if (card.type == "C") {
                card.rung = "♣";
                clubs.push(card);
            }
            else if (card.type == "D") {
                card.rung = "♦";
                diamonds.push(card);
            }
        }
        spades = spades.sort(function (a, b) { return (a.points > b.points) ? 1 : ((b.points > a.points) ? -1 : 1); });
        hearts = hearts.sort(function (a, b) { return (a.points > b.points) ? 1 : ((b.points > a.points) ? -1 : 1); });
        clubs = clubs.sort(function (a, b) { return (a.points > b.points) ? 1 : ((b.points > a.points) ? -1 : 1); });
        diamonds = diamonds.sort(function (a, b) { return (a.points > b.points) ? 1 : ((b.points > a.points) ? -1 : 1); });
        cards = cards.concat(clubs);
        cards = cards.concat(diamonds);
        cards = cards.concat(spades);
        cards = cards.concat(hearts);
        var xinc = (width - 35) / cards.length;
        if (xinc > 35) {
            xinc = 35;
        }
        if (!isFirstPlayer) {
            xinc = 0;
        }
        var x = (width - (xinc * player.cards.length + 45)) / 2;
        var ct = 0;
        for (var _b = 0, cards_1 = cards; _b < cards_1.length; _b++) {
            var card = cards_1[_b];
            if (xinc == 0) {
                card.x = 0;
            }
            else {
                card.x = x;
            }
            card.y = 0;
            card.rotation = degree;
            card.opacity = opacity;
            x = x + xinc;
            ct++;
        }
        player.playerCards = cards;
        return cards;
    };
    return Deck;
}());

//# sourceMappingURL=deck.js.map

/***/ }),

/***/ 628:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Turn; });
var Turn = /** @class */ (function () {
    function Turn() {
    }
    return Turn;
}());

//# sourceMappingURL=turn.js.map

/***/ }),

/***/ 629:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Hand; });
var Hand = /** @class */ (function () {
    function Hand() {
    }
    return Hand;
}());

//# sourceMappingURL=hand.js.map

/***/ }),

/***/ 630:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Message; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_moment__);

var Message = /** @class */ (function () {
    function Message() {
    }
    Object.defineProperty(Message.prototype, "time", {
        get: function () {
            if (this.lastUpdateTime) {
                return __WEBPACK_IMPORTED_MODULE_0_moment___default()(this.lastUpdateTime.toDate()).format("hh:mm a");
            }
            else {
                return "Sending";
            }
        },
        enumerable: true,
        configurable: true
    });
    return Message;
}());

//# sourceMappingURL=message.js.map

/***/ }),

/***/ 636:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CallingService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__storageservice__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CallingService = /** @class */ (function () {
    function CallingService(storageService) {
        this.storageService = storageService;
        this.ICE_SERVERS = [{ url: "stun:stun.l.google.com:19302" }];
        this.peers = {};
        this.local_media_stream = null;
    }
    CallingService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__storageservice__["a" /* StorageService */]])
    ], CallingService);
    return CallingService;
}());

//# sourceMappingURL=callingService.js.map

/***/ }),

/***/ 637:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArraySortPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ArraySortPipe = /** @class */ (function () {
    function ArraySortPipe() {
    }
    ArraySortPipe.prototype.transform = function (array, field) {
        if (!Array.isArray(array)) {
            return;
        }
        array.sort(function (a, b) {
            if (a[field] < b[field]) {
                return -1;
            }
            else if (a[field] > b[field]) {
                return 1;
            }
            else {
                return 0;
            }
        });
        return array;
    };
    ArraySortPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: "sort"
        })
    ], ArraySortPipe);
    return ArraySortPipe;
}());

//# sourceMappingURL=arraySortPipe.js.map

/***/ }),

/***/ 638:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__voicenote_voicenote__ = __webpack_require__(639);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_long_press__ = __webpack_require__(424);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__voicenote_voicenote__["a" /* VoicenoteComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */], __WEBPACK_IMPORTED_MODULE_3_ionic_long_press__["a" /* LongPressModule */]],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_1__voicenote_voicenote__["a" /* VoicenoteComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_1__voicenote_voicenote__["a" /* VoicenoteComponent */]],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 639:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoicenoteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_recordrtc__ = __webpack_require__(640);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_recordrtc___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_recordrtc__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(425);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




/**
 * Generated class for the VoicenoteComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var VoicenoteComponent = /** @class */ (function () {
    function VoicenoteComponent(httpClient) {
        this.httpClient = httpClient;
        this.seconds = 0;
        this.onFileUpload = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
    }
    Object.defineProperty(VoicenoteComponent.prototype, "time", {
        get: function () {
            if (this.seconds) {
                return __WEBPACK_IMPORTED_MODULE_2_moment___default()(0).seconds(this.seconds).format("mm:ss");
            }
            else {
                return "";
            }
        },
        enumerable: true,
        configurable: true
    });
    VoicenoteComponent.prototype.startRecording = function () {
        var mediaConstraints = {
            video: false,
            audio: true
        };
        navigator.mediaDevices
            .getUserMedia(mediaConstraints)
            .then(this.successCallback.bind(this), this.errorCallback.bind(this));
    };
    VoicenoteComponent.prototype.successCallback = function (stream) {
        var _this = this;
        this.interval = setInterval(function () {
            _this.seconds++;
        }, 1000);
        var options = {
            mimeType: 'audio/webm',
            audioBitsPerSecond: 128000,
            bitsPerSecond: 128000 // if this line is provided, skip above two
        };
        this.stream = stream;
        this.recordRTC = __WEBPACK_IMPORTED_MODULE_1_recordrtc__(stream, options);
        this.recordRTC.startRecording();
    };
    VoicenoteComponent.prototype.errorCallback = function () {
        //handle error here
    };
    VoicenoteComponent.prototype.stopRecording = function () {
        clearInterval(this.interval);
        this.seconds = 0;
        var recordRTC = this.recordRTC;
        if (recordRTC) {
            recordRTC.stopRecording(this.processVideo.bind(this));
            var stream = this.stream;
            stream.getAudioTracks().forEach(function (track) { return track.stop(); });
            stream.getVideoTracks().forEach(function (track) { return track.stop(); });
        }
    };
    VoicenoteComponent.prototype.processVideo = function (audioVideoWebMURL) {
        return __awaiter(this, void 0, void 0, function () {
            var recordRTC, recordedBlob, formData;
            var _this = this;
            return __generator(this, function (_a) {
                recordRTC = this.recordRTC;
                recordedBlob = recordRTC.getBlob();
                formData = new FormData();
                //formData.append("UPLOADCARE_PUB_KEY","566093482b2b3c428983");
                //formData.append("UPLOADCARE_STORE","0");
                formData.append("file", recordedBlob);
                formData.append("name", new Date().getTime().toString());
                this.httpClient.post("uguu/", formData, { responseType: "text" }).subscribe(function (data) {
                    _this.onFileUpload.emit(data);
                }, function (error) {
                });
                return [2 /*return*/];
            });
        });
    };
    VoicenoteComponent.prototype.ngAfterContentInit = function () {
        var mediaConstraints = {
            video: false,
            audio: true
        };
        navigator.mediaDevices
            .getUserMedia(mediaConstraints)
            .then(function (stream) { }, function (error) { });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", Object)
    ], VoicenoteComponent.prototype, "onFileUpload", void 0);
    VoicenoteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'voicenote',template:/*ion-inline-start:"C:\Users\one\Desktop\projects\martor\src\components\voicenote\voicenote.html"*/'<!-- Generated template for the VoicenoteComponent component -->\n\n<ion-icon name="microphone"  ion-long-press [interval]="100" (onPressStart)="startRecording()"  (onPressEnd)="stopRecording()" ></ion-icon>\n\n<span *ngIf="seconds>0" style="position: absolute;left:-160px;font-size:18px;background-color:white;color:black;">{{time}}</span>'/*ion-inline-end:"C:\Users\one\Desktop\projects\martor\src\components\voicenote\voicenote.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], VoicenoteComponent);
    return VoicenoteComponent;
}());

//# sourceMappingURL=voicenote.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_session__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__helper_fireStoreHelper__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_angularfire2_firestore__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_angularfire2_firestore___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__node_modules_angularfire2_firestore__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_user__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__models_player__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_firebase__ = __webpack_require__(295);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_firebase___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_firebase__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var StorageService = /** @class */ (function () {
    function StorageService(db, events) {
        this.db = db;
        this.events = events;
    }
    StorageService.prototype.setProperty = function (key, value) {
        if (value) {
            window.localStorage.setItem(key, JSON.stringify(value));
        }
    };
    StorageService.prototype.getProperty = function (key) {
        if (window.localStorage.getItem(key)) {
            return JSON.parse(window.localStorage.getItem(key));
        }
        return null;
    };
    StorageService.prototype.getStrProperty = function (key) {
        return window.localStorage.getItem(key);
    };
    StorageService.prototype.removeAllProperty = function () {
        window.localStorage.clear();
    };
    StorageService.prototype.removeProperty = function (key) {
        window.localStorage.removeItem(key);
    };
    StorageService.prototype.getSession = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var exists;
                        var _this = this;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    this.sessionId = this.getProperty("sessionId");
                                    if (!this.sessionId) return [3 /*break*/, 2];
                                    return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__helper_fireStoreHelper__["a" /* FireStoreHelper */].checkIfDocumentExists(this.db, "session", this.sessionId)];
                                case 1:
                                    exists = _a.sent();
                                    if (this.session == null) {
                                        this.session = new __WEBPACK_IMPORTED_MODULE_1__models_session__["a" /* Session */]();
                                    }
                                    if (exists) {
                                        if (!this.sessionSubscription) {
                                            this.sessionObservable = __WEBPACK_IMPORTED_MODULE_2__helper_fireStoreHelper__["a" /* FireStoreHelper */].subscribeDocument(this.db, "session", this.sessionId);
                                            this.sessionSubscription = this.sessionObservable.subscribe(function (data) {
                                                _this.updateSession(data);
                                            });
                                        }
                                        resolve(this.session);
                                    }
                                    else {
                                        this.removeProperty("sessionId");
                                        this.sessionId = "";
                                        resolve(null);
                                    }
                                    return [3 /*break*/, 3];
                                case 2:
                                    resolve(null);
                                    _a.label = 3;
                                case 3: return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    StorageService.prototype.getBotSession = function () {
        var session = new __WEBPACK_IMPORTED_MODULE_1__models_session__["a" /* Session */]();
        session.id = "botsession";
        session.gameId = "botgame";
        session.createdBy = this.getUser(false).id;
        session.lastUpdateTime = { toDate: function () {
                return new Date();
            } };
        var player = new __WEBPACK_IMPORTED_MODULE_6__models_player__["a" /* Player */]();
        player.id = this.getUser(false).id;
        player.name = "You";
        if (this.getUser(false).name) {
            player.name = this.getUser(false).name;
        }
        player.index = 0;
        var bot1 = new __WEBPACK_IMPORTED_MODULE_6__models_player__["a" /* Player */]();
        bot1.id = "1";
        bot1.index = 1;
        bot1.name = "Shakur";
        bot1.isBot = true;
        var bot2 = new __WEBPACK_IMPORTED_MODULE_6__models_player__["a" /* Player */]();
        bot2.id = "2";
        bot2.index = 2;
        bot2.name = "Razzaq";
        bot2.isBot = true;
        session.players = [];
        session.players.push(player);
        session.players.push(bot1);
        session.players.push(bot2);
        return session;
    };
    StorageService.prototype.updateSession = function (data) {
        if (data && data.lastUpdateTime) {
            this.command == data.command;
            var oldGameId = this.session.gameId;
            this.session = Object.assign(this.session, data);
            // if (!data.gameId) {
            //     this.events.publish("exit-game", '');
            // }
            // else if (data.gameId && data.gameId !== oldGameId) {
            //     this.events.publish("new-game", this.session.gameId);
            // }
            //Change In Umer's Code
        }
    };
    StorageService.prototype.setUser = function (user) {
        this.setProperty("user", user);
    };
    StorageService.prototype.setSession = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                        var exists;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    this.resetSession();
                                    return [4 /*yield*/, __WEBPACK_IMPORTED_MODULE_2__helper_fireStoreHelper__["a" /* FireStoreHelper */].checkIfDocumentExists(this.db, "session", id)];
                                case 1:
                                    exists = _a.sent();
                                    if (exists) {
                                        this.sessionId = id;
                                        this.setProperty("sessionId", id);
                                        this.getSession();
                                        resolve(true);
                                    }
                                    else {
                                        resolve(false);
                                    }
                                    return [2 /*return*/];
                            }
                        });
                    }); })];
            });
        });
    };
    StorageService.prototype.resetSession = function () {
        this.sessionId = '';
        if (this.sessionSubscription) {
            this.sessionSubscription.unsubscribe();
            this.sessionSubscription = null;
        }
        this.removeProperty("sessionId");
        this.session = null;
    };
    StorageService.prototype.getUser = function (appComponent) {
        if (!this.user) {
            var obj = this.getProperty("user");
            if (obj) {
                this.user = Object.assign(new __WEBPACK_IMPORTED_MODULE_4__models_user__["a" /* User */](), obj);
            }
            else {
                this.user = new __WEBPACK_IMPORTED_MODULE_4__models_user__["a" /* User */]();
                this.user.id = new Date().getTime().toString();
                this.setProperty("user", this.user);
                this.db.collection("users").doc(this.user.id).set({ points: 30000, gameId: "" }, { merge: true });
            }
        }
        if (appComponent) {
            this.onlineStatus();
        }
        if (!appComponent) {
            return this.user;
        }
    };
    StorageService.prototype.onlineStatus = function () {
        var _this = this;
        var firestoreDb = __WEBPACK_IMPORTED_MODULE_8_firebase__["firestore"]();
        var oldRealTimeDb = __WEBPACK_IMPORTED_MODULE_8_firebase__["database"]();
        var usersRef = firestoreDb.collection('users'); // Get a reference to the Users collection;
        var onlineRef = oldRealTimeDb.ref('.info/connected'); // Get a reference to the list of connections
        onlineRef.on('value', function (snapshot) {
            oldRealTimeDb
                .ref("/status/" + _this.user.id)
                .onDisconnect()
                .set('offline')
                .then(function () {
                _this.user.online = true;
                _this.user["lastUpdateTimeClient"] = __WEBPACK_IMPORTED_MODULE_7_moment___default()(new Date()).utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]");
                __WEBPACK_IMPORTED_MODULE_2__helper_fireStoreHelper__["a" /* FireStoreHelper */].setWithCallback(_this.db, "users", _this.user, _this.user.id).then(function () {
                    __WEBPACK_IMPORTED_MODULE_2__helper_fireStoreHelper__["a" /* FireStoreHelper */].get(_this.db, "users", _this.user.id).then(function (data) {
                        var userdto = data.data();
                        _this.user = Object.assign(userdto, _this.user);
                        _this.user.id = data.id;
                        _this.user.points = userdto.points;
                        var clientTime = userdto["lastUpdateTimeClient"];
                        if (userdto["lastUpdateTime"]) {
                            var dateObj = new Date(userdto["lastUpdateTime"].toDate()); //error
                            var serverTime = __WEBPACK_IMPORTED_MODULE_7_moment___default()(dateObj).toISOString();
                            _this.user.timeDifference = Math.round((__WEBPACK_IMPORTED_MODULE_7_moment___default()(clientTime).diff(__WEBPACK_IMPORTED_MODULE_7_moment___default()(serverTime))) / 1000);
                        }
                    });
                });
                oldRealTimeDb.ref("/status/" + _this.user.id).set('online');
            });
        });
    };
    StorageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__node_modules_angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__node_modules_angularfire2_firestore__["AngularFirestore"],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["c" /* Events */]])
    ], StorageService);
    return StorageService;
}());

//# sourceMappingURL=storageservice.js.map

/***/ })

},[426]);
//# sourceMappingURL=main.js.map